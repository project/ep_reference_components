
/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */
var config = (function (drupalSettings) {
  return {
    cortexApi: {
      path: drupalSettings.epConfig.cortexApiEndpoint,
      scope: drupalSettings.epConfig.cortexScope,
      pathForProxy: '',
    },
    supportedLocales: [
      {
        value: 'en-CA',
        name: 'English',
      },
      {
        value: 'it-IT',
        name: 'Italian',
      },
    ],
    indi: {
      enable: false,
      carousel: {
        apikey: '',
        id: '',
        size: 'large',
        theme: 'large',
        round_corners: false,
        show_title: false,
        show_views: false,
        show_likes: false,
        show_buzz: false,
        animate: true,
      },
      productReview: {
        submit_button_url: '',
        thumbnail_url: '',
      },
      brandAmbassador: {
        submit_button_url: '',
        thumbnail_url: '',
      },
    },
    defaultLocaleValue: drupalSettings.epConfig.defaultLocale,
    supportedCurrencies: [
      {
        value: 'USD',
        name: 'US Dollars',
      },
      {
        value: 'EUR',
        name: 'Euro',
      },
    ],
    b2b: {
      enable: drupalSettings.epConfig.b2b.useB2B,
      authServiceAPI: {
        path: drupalSettings.epConfig.b2b.authServiceApi,
        pathForProxy: ""
      },
      keycloak: {
        callbackUrl: drupalSettings.epConfig.b2b.authCallbackUrl,
        loginRedirectUrl: drupalSettings.epConfig.b2b.authLoginRedirectUrl,
        logoutRedirectUrl: drupalSettings.epConfig.b2b.authLogoutRedirectUrl,
        client_id: "eam",
      },
    },
    defaultCurrencyValue: drupalSettings.epConfig.defaultCurrency,
    skuImagesUrl: drupalSettings.epConfig.skuImagesUrl,
    siteImagesUrl: drupalSettings.epConfig.siteImagesUrl,
    enableOfflineMode: false,
    gaTrackingId: '',
    arKit: {
      enable: true,
      skuArImagesUrl: drupalSettings.epConfig.skuArImagesUrl,
    },
  };
})(drupalSettings);