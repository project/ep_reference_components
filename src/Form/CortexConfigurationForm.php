<?php

namespace Drupal\ep_reference_components\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * CortexConfigurationForm.  Entrypoint into the configuration service.
 */
class CortexConfigurationForm extends FormBase {

  /**
   * Module settings.
   *
   * @var settings
   */
  protected $settings;

  /**
   * Ctor.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->settings = $configFactory->getEditable('ep_reference_components.settings');
  }

  /**
   * Create an instance from a service container.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Gets the form ID.
   *
   * @{inheritdoc}
   */
  public function getFormId() {
    return 'ep_reference_components.cortex_configuration';
  }

  /**
   * Submit the form and rebuild the configuration.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->settings->set('cortexApiEndpoint', $form_state->getValue('cortexApiEndpoint'));
    $this->settings->set('cortexScope', $form_state->getValue('cortexScope'));
    $this->settings->set('defaultLocale', $form_state->getValue('defaultLocale'));
    $this->settings->set('defaultCurrency', $form_state->getValue('defaultCurrency'));
    $this->settings->set('skuImagesUrl', $form_state->getValue('skuImagesUrl'));
    $this->settings->set('siteImagesUrl', $form_state->getValue('siteImagesUrl'));
    $this->settings->set('skuArImagesUrl', $form_state->getValue('skuArImagesUrl'));
    $this->settings->set('useB2B', $form_state->getValue('useB2B'));

    if ($form_state->getValue('useB2B')) {
      $this->settings->set('authServiceApi', $form_state->getValue('authServiceApi'));
      $this->settings->set('authCallbackUrl', $form_state->getValue('authCallbackUrl'));
      $this->settings->set('authLoginRedirectUrl', $form_state->getValue('authLoginRedirectUrl'));
      $this->settings->set('authLogoutRedirectUrl', $form_state->getValue('authLogoutRedirectUrl'));
    }

    $this->settings->save();

    drupal_set_message('The configuration has been saved');
  }

  /**
   * Build the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['cortexApiEndpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cortex API Enpoint'),
      '#required' => TRUE,
      '#default_value' => $this->settings->get('cortexApiEndpoint'),
    ];

    $form['cortexScope'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scope'),
      '#required' => TRUE,
      '#default_value' => $this->settings->get('cortexScope'),
    ];

    $form['defaultLocale'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Locale'),
      '#required' => TRUE,
      '#default_value' => $this->settings->get('defaultLocale'),
    ];

    $form['defaultCurrency'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Currency'),
      '#required' => TRUE,
      '#default_value' => $this->settings->get('defaultCurrency'),
    ];

    $form['skuImagesUrl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sku Images URL'),
      '#required' => TRUE,
      '#default_value' => $this->settings->get('skuImagesUrl'),
    ];

    $form['siteImagesUrl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site Images URL'),
      '#required' => TRUE,
      '#default_value' => $this->settings->get('siteImagesUrl'),
    ];

    $form['skuArImagesUrl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AR Images URL'),
      '#required' => TRUE,
      '#default_value' => $this->settings->get('skuArImagesUrl'),
    ];

    $form['useB2B'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use B2B features?'),
      '#required' => FALSE,
      '#default_value' => $this->settings->get('useB2B'),
    ];

    $form['authServiceApi'] = [
      '#type' => 'textfield',
      '#title' => $this->t('B2B Auth service API url'),
      '#required' => FALSE,
      '#default_value' => $this->settings->get('authServiceApi'),
    ];

    $form['authCallbackUrl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('B2B callback URL'),
      '#required' => FALSE,
      '#default_value' => $this->settings->get('authCallbackUrl'),
    ];

    $form['authLoginRedirectUrl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('B2B keycloak auth URL'),
      '#required' => FALSE,
      '#default_value' => $this->settings->get('authLoginRedirectUrl'),
    ];

    $form['authLogoutRedirectUrl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('B2B keycloak logout URL'),
      '#required' => FALSE,
      '#default_value' => $this->settings->get('authLogoutRedirectUrl'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
    ];

    return $form;
  }

  /**
   * Validates the form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if ($form_state->getValue('useB2B')) {

      $required_fields = [
        'authServiceApi', 'authCallbackUrl', 'authLoginRedirectUrl', 'authLogoutRedirectUrl',
      ];

      foreach ($required_fields as $field) {
        if (empty($form_state->getValue($field))) {
          $form_state->setErrorByName($field, $this->t('This field is required if using B2B features.'));
        }
      }
    }
  }

}
