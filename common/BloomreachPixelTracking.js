
/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

function initializeBRPixelTrackingData() {
  window.br_data = window.br_data || {};
  window.br_data.acct_id = '6226';
  window.br_data.domain_key = 'elastic_path';
  window.br_data.view_id = '<BloomReach View ID>';
  window.br_data.user_id = 'uid%3D8030319374785%3Av%3D11.8%3Ats%3D1536857546532%3Ahc%3D111';
  window.br_data.orig_ref_url = window.location.href;
}

function executePixelTrackingScript(callback) {
  const script = document.createElement('script');
  script.type = 'text/javascript';
  script.async = true;
  script.src = 'http://cdn.brcdn.com/v1/br-trk-6226.js';
  script.onload = callback || null;
  const head = document.getElementsByTagName('head')[0];
  head.appendChild(script);
}

export function logProductPageVisit(skuCode) {
  initializeBRPixelTrackingData();
  window.br_data.sku = skuCode;
  window.br_data.ptype = 'product';
  executePixelTrackingScript();
}

export function logProductPageAddToCart(skuCode) {
  window.BrTrk.getTracker().logEvent('cart', 'click-add', { prod_id: skuCode, sku: skuCode });
}

export function logSearchPageVisit(keyword) {
  initializeBRPixelTrackingData();
  window.br_data.search_term = `Keyword: ${keyword}`;
  window.br_data.ptype = 'search';
  executePixelTrackingScript();
}

export function logSearchBarEvent(keywords) {
  initializeBRPixelTrackingData();
  executePixelTrackingScript(() => {
    const searchData = {};
    searchData.q = keywords;
    window.BrTrk.getTracker().logEvent('suggest', 'submit', searchData, {}, true);
  });
}
