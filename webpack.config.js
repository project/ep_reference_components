/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

const path = require('path');

module.exports = {
  entry: {
    about_us_page: ['./containers/about_us_page/about_us.js'],
    batch_add_to_cart: ['./containers/batch_add_to_cart/batch_add_to_cart.js'],
    batch_quick_order_entry: ['./containers/batch_quick_order_entry/batch_quick_order_entry.js'],
    banner_generic: ['./containers/banner_generic/banner_generic.js'],
    cart_commercial: ['./containers/cart_commercial/cart_commercial.js'],
    cart_page: ['./containers/cart_page/cart.js'],
    category_page: ['./containers/category_page/category.js'],
    category_page_commercial: ['./containers/category_page_commercial/category.js'],
    checkout_auth_page: ['./containers/checkout_auth_page/checkout_auth.js'],
    checkout_commercial: ['./containers/checkout_commercial/checkout_commercial.js'],
    checkout_page: ['./containers/checkout_page/checkout.js'],
    contact_us_page: ['./containers/contact_us_page/contact_us.js'],
    edit_address_page: ['./containers/edit_address_page/edit_address.js'],
    featured_category: ['./containers/featured_category/featured_category.js'],
    featured_parallax: ['./containers/featured_parallax/featured_parallax.js'],
    featured_product: ['./containers/featured_product/featured_product.js'],
    image_carousel: ['./containers/image_carousel/image_carousel.js'],
    maintenance_page: ['./containers/maintenance_page/maintenance_page.js'],
    new_address_page: ['./containers/new_address_page/new_address.js'],
    new_payment_page: ['./containers/new_payment_page/new_payment.js'],
    order_history_page: ['./containers/order_history_page/order_history.js'],
    order_history_page_commercial: ['./containers/order_history_page_commercial/order_history.js'],
    order_review_page: ['./containers/order_review_page/order_review.js'],
    order_review_commercial: ['./containers/order_review_commercial/order_review_commercial.js'],
    pay_by_po: ['./containers/pay_by_po/pay_by_po.js'],
    product_compare: ['./containers/product_compare/product_compare.js'],
    product_detail: ['./containers/product_detail/product_detail.js'],
    profile_page: ['./containers/profile_page/profile_page.js'],
    purchase_receipt_page: ['./containers/purchase_receipt_page/purchase_receipt.js'],
    purchase_receipt_commercial: ['./containers/purchase_receipt_commercial/purchase_receipt_commercial.js'],
    quick_order_entry: ['./containers/quick_order_entry/quick_order_entry.js'],
    registration_page: ['./containers/registration_page/registration_page.js'],
    search_results: ['./containers/search_results/search_results.js'],
    search_results_commercial: ['./containers/search_results_commercial/search_results_commercial.js'],
    terms_and_conditions_page: ['./containers/terms_and_conditions_page/terms_and_conditions_page.js'],
    shipping_returns: ['./containers/shipping_returns/shipping_returns.js'],
    wishlist_page: ['./containers/wishlist_page/wishlist.js'],
    app_header_search: ['./containers/app_header_search/app_header_search.js'],
    app_header_locale: ['./containers/app_header_locale/app_header_locale.js'],
    app_header_main: ['./containers/app_header_main/app_header_main.js'],
    app_header_hamburger_btn: ['./containers/app_header_hamburger_btn/app_header_hamburger_btn.js'],
    app_header_login: ['./containers/app_header_login/app_header_login.js'],
    app_header_logo: ['./containers/app_header_logo/app_header_logo.js'],
    app_header_top: ['./containers/app_header_top/app_header_top.js'],
    app_header_cart: ['./containers/app_header_cart/app_header_cart.js'],
    app_modal_login_container: ['./containers/app_modal_login_container/app_modal_login_container.js'],
    app_modal_cart_select: ['./containers/app_modal_cart_select/app_modal_cart_select.js'],
    app_footer: ['./containers/app_footer/app_footer.js'],
  },
  externals: {
    react: 'React',
    'react-dom': 'ReactDOM',
    Drupal: 'Drupal',
    config: 'config',
  },
  output: {
    path: path.resolve(__dirname, 'containers'),
    filename: '[name]/[name].bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
        ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          { loader: 'babel-loader' },
        ],
      },
      {
        test: /\.js$/,
        enforce: 'pre',
        exclude: /node_modules/,
        use: [
          { loader: 'eslint-loader' },
        ],
      },
      {
        test: /\.less$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'less-loader' },
        ],
      },
      {
        test: /\.(png|jp(e*)g|svg|gif|jp2)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 8000, // Convert images < 8kb to base64 strings
            name: '[hash]-[name].[ext]',
            publicPath: '/images/temp-webpack-generated-images/',
            outputPath: '../../../../images/temp-webpack-generated-images',
          },
        }],
      },
      {
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/',
          },
        }],
      },
    ],
  },
  stats: {
    colors: true,
  },
  devtool: 'source-map',
};
