/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import withDataAttributes from '../../components/util/dataattribute';

const fallbackLogoImageUrl = 'https://s3-us-west-2.amazonaws.com/elasticpath-demo-images/VESTRI_VIRTUAL/Company-Logo-v1.png';

const AppheaderlogoMain = (props) => {
  const { dataSet } = props;
  const { logoImageUrl, allowedLogoHeight } = dataSet;

  return (
    <div className="logo-container">
      <a href="/" className="logo">
        <img
          className="logo-image"
          style={allowedLogoHeight ? { height: `${allowedLogoHeight}px` } : undefined}
          alt="Header logo"
          src={logoImageUrl || fallbackLogoImageUrl}
          onError={(e) => { e.target.src = fallbackLogoImageUrl; }}
        />
      </a>
    </div>
  );
};

AppheaderlogoMain.propTypes = {
  dataSet: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default withDataAttributes(AppheaderlogoMain);
