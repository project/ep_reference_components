/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import intl from 'react-intl-universal';
import Config from 'config';
import Papa from 'papaparse';
import { login } from '../../common/AuthService';
import { cortexFetch } from '../../common/Cortex';


// Array of zoom parameters to pass to Cortex
const zoomArray = [
  'defaultcart:batch-add-to-cart-form',
];

class BatchAddToCart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cartData: undefined,
    };
  }

  componentDidMount() {
    this.fetchCartData();
  }

  componentWillReceiveProps() {
    this.fetchCartData();
  }

  // eslint-disable-next-line class-methods-use-this
  batchAddToCart(list) {
    const body = {
      items: list,
    };
    const { cartData } = this.state;
    login().then(() => {
      const addToCartLink = cartData['_batch-add-to-cart-form'][0].links.find(link => link.rel === 'submit-action');
      cortexFetch(addToCartLink.uri,
        {
          method: 'post',
          headers: {
            'Content-Type': 'application/json',
            Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
          },
          body: JSON.stringify(body),
        })
        .then((res) => {
          if (res.status === 200 || res.status === 201) {
            window.location = '/mybag';
          } else {
            let debugMessages = '';
            res.json().then((json) => {
              for (let i = 0; i < json.messages.length; i++) {
                debugMessages = debugMessages.concat(`- ${json.messages[i]['debug-message']} \n `);
              }
              document.getElementById('batch-add-to-cart-error').innerHTML = debugMessages;
            });
          }
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error.message);
        });
    }).catch((error) => {
      // eslint-disable-next-line no-console
      console.error(error.message);
    });
  }

  fetchCartData() {
    login().then(() => {
      cortexFetch(`/?zoom=${zoomArray.sort().join()}`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
        },
      })
        .then(res => res.json())
        .then((res) => {
          this.setState({
            cartData: res._defaultcart[0],
          });
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error.message);
        });
    });
  }


  // eslint-disable-next-line class-methods-use-this
  processCSV() {
    const file = document.getElementById('batch-csv').files[0];
    if (file) {
      Papa.parse(file, {
        header: true,
        dynamicTyping: true,
        encoding: 'utf-8',
        skipEmptyLines: true,
        // eslint-disable-next-line func-names
        complete: (function (results) {
          // eslint-disable-next-line no-console
          console.log(results.data);
          this.batchAddToCart(results.data);
        }).bind(this),
      });
    }
  }


  render() {
    return (
      <div className="batch-add-to-cart-container container">
        <h1 className="subtitle-label">{intl.get('batch-add-to-cart')}</h1>
        <div className="error-label" id="batch-add-to-cart-error" />
        { /* eslint-disable-next-line no-param-reassign */ }
        <input type="file" className="btn-batch-csv-hidden" onClick={(event) => { event.target.value = null; }} onChange={() => this.processCSV()} name="file" id="batch-csv" accept=".csv" />
        { /* eslint-disable-next-line jsx-a11y/label-has-associated-control */ }
        <label className="ep-btn primary wide btn-batch-csv" htmlFor="batch-csv">{intl.get('upload-csv')}</label>
      </div>
    );
  }
}

export default BatchAddToCart;
