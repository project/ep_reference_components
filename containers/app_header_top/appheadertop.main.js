/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import intl from 'react-intl-universal';

class AppHeaderTop extends React.Component {
  static propTypes = {
    isMobileView: PropTypes.bool.isRequired,
  }


  render() {
    const { isMobileView } = this.props;
    return [
      <div className={`top-header ${isMobileView ? 'mobile-view' : ''}`}>
        <div className="top-container">
          <div className="top-container-menu">
            <ul>
              <li>
                <a href="/shippingreturns">
                  {intl.get('shipping-returns')}
                </a>
              </li>
              <li>
                <a href="/aboutus">
                  {intl.get('help')}
                </a>
              </li>
              <li>
                <a href="/contactus">
                  {intl.get('contact')}
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>,
    ];
  }
}

export default AppHeaderTop;
