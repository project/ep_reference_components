/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import ReactRouterPropTypes from 'react-router-prop-types';
import intl from 'react-intl-universal';
import OrderTableMainCommercial from '../../components/ordertable_main_commercial/ordertable.main.commercial';
import PaymentMethodContainer from '../../components/paymentmethod_container/paymentmethod.container';
import ShippingOptionContainer from '../../components/shippingoption_container/shippingoption.container';
import AddressContainer from '../../components/address_container/address.container';

const PurchaseReceiptCommercial = (props) => {
  const { location } = props;
  const orderData = location.state.data;
  const { status } = orderData;
  let statusString;
  switch (status) {
    case 'CANCELED':
      statusString = intl.get('cancelled');
      break;
    case 'COMPLETED':
      statusString = intl.get('completed');
      break;
    default:
      statusString = intl.get('in-progress');
  }
  const orderNumber = orderData['purchase-number'];
  const orderPurchaseDate = orderData['purchase-date']['display-value'];
  const shipments = orderData._shipments;

  const calculateSubtotal = () => {
    const currencySymbol = Number.isNaN(parseInt(orderData['monetary-total'][0].display.charAt(0), 10)) ? orderData['monetary-total'][0].display.charAt(0) : '';
    return currencySymbol + (orderData['monetary-total'][0].amount
      - orderData['tax-total'].amount
      - orderData._discount[0].discount[0].amount
      - orderData._shipments[0]._element[0]._shippingoption[0].cost[0].amount).toFixed(2);
  };

  const renderShippingOption = () => {
    if (shipments) {
      const [option] = orderData._shipments[0]._element[0]._shippingoption;
      return (
        <div>
          <h3 className="shipping-billing-title">
            {intl.get('shipping-option')}
          </h3>
          <ShippingOptionContainer option={option} />
        </div>
      );
    }
    return null;
  };

  const renderShippingAddress = () => {
    if (shipments) {
      const [shippingAddress] = orderData._shipments[0]._element[0]._destination;
      const { name, address } = shippingAddress;
      return (
        <div>
          <h3 className="shipping-billing-title">
            {intl.get('shipping-address')}
          </h3>
          <AddressContainer name={name} address={address} />
        </div>
      );
    }
    return null;
  };

  const renderBillingAddress = () => {
    const [billingAddress] = orderData._billingaddress;
    const { name, address } = billingAddress;
    return (
      <div>
        <h3 className="shipping-billing-title">
          {intl.get('billing-address')}
        </h3>
        <AddressContainer name={name} address={address} />
      </div>
    );
  };

  const renderPaymentMethod = () => {
    const displayName = orderData._paymentmeans[0]._element[0]['display-name'];
    return (
      <div>
        <h3 className="shipping-billing-title">
          {intl.get('payment-method')}
        </h3>
        <PaymentMethodContainer displayName={displayName} />
      </div>
    );
  };

  const renderDetails = () => {
    const orderTaxTotal = orderData['tax-total'].display;
    const orderDiscount = orderData._discount[0].discount[0].display;
    const orderTotal = orderData['monetary-total'][0].display;
    return (
      <div className="checkout-sidebar">
        <div className="checkout-sidebar-inner">
          <div className="d-md-flex flex-row flex-md-row-reverse justify-content-between">
            <div className="checkout-summary-container">
              <div className="d-flex flex-row justify-content-between">
                <div>
                  {intl.get('todays-subtotal')}
                </div>
                <div>
                  {calculateSubtotal()}
                </div>
              </div>
              <div className="d-flex flex-row justify-content-between">
                <div>
                  {intl.get('order-shipping')}
                </div>
                <div>
                  {orderData._shipments[0]._element[0]._shippingoption[0].cost[0].display}
                </div>
              </div>
              <div className="d-flex flex-row justify-content-between">
                <div>
                  {intl.get('todays-discount')}
                </div>
                <div>
                  {orderDiscount}
                </div>
              </div>
              <div className="d-flex flex-row justify-content-between">
                <div>
                  {intl.get('order-tax-total')}
                </div>
                <div>
                  {orderTaxTotal}
                </div>
              </div>
              <div className="d-flex flex-row justify-content-between purchase-total">
                <div>
                  {intl.get('order-total')}
                </div>
                <div>
                  {orderTotal}
                </div>
              </div>
            </div>
            <div className="d-flex flex-column justify-content-end">
              <button className="ep-btn secondary wide" type="button" onClick={() => { window.print(); }}>
                {intl.get('print-confirmation')}
              </button>
              <button className="ep-btn primary wide" type="button" onClick={() => { window.location.href = '/'; }}>
                {intl.get('continue-shopping')}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div>
      <div className="order-commercial container">
        <div className="order-commercial-inner">
          <div className="order-title-container">
            <div>
              <h1 className="view-title">
                {intl.get('order-confirmation')}
              </h1>
              <div className="order-confirmation">
                <div>
                  {intl.get('order-number')}
                  :&nbsp;
                  {orderNumber}
                </div>
                <div>
                  {intl.get('status')}
                  :&nbsp;
                  {statusString}
                </div>
                <div>
                  {intl.get('order-purchase-date')}
                  :&nbsp;
                  {orderPurchaseDate}
                </div>
              </div>
            </div>
            <div className="order-title-row row">
              <div className="col-md order-col-prd text-uppercase">
                {intl.get('product')}
              </div>
              <div className="col-md" />
              <div className="col-md order-col-availability text-uppercase">
                {intl.get('availability')}
              </div>
              <div className="col-md order-col-qty text-uppercase">
                {intl.get('quantity')}
              </div>
              <div className="col-md order-col-each text-uppercase">
                {intl.get('price')}
              </div>
              <div className="col-md order-col-total text-uppercase">
                {intl.get('total')}
              </div>
            </div>
          </div>
          {orderData && (
            <div className="order-main-container">
              <div className="order-items-container">
                <OrderTableMainCommercial data={orderData} />
              </div>
              <div className="container order-section">
                <div className="order-label-row">
                  {intl.get('order-shipping')}
                </div>
                <div className="shipping-info row">
                  <div className="col-6 col-md-4">
                    {renderShippingAddress()}
                  </div>
                  <div className="col-6 col-md-8">
                    {renderShippingOption()}
                  </div>
                </div>
              </div>
              <div className="container order-section">
                <div className="order-label-row">
                  {intl.get('order-billing')}
                </div>
                <div className="billing-info row">
                  <div className="col-6 col-md-4">
                    {renderBillingAddress()}
                  </div>
                  <div className="col-6 col-md-8">
                    {renderPaymentMethod()}
                  </div>
                </div>
              </div>
            </div>
          )}
          {orderData && (
            renderDetails()
          )}
        </div>
      </div>
    </div>
  );
};

PurchaseReceiptCommercial.propTypes = {
  location: ReactRouterPropTypes.location.isRequired,
};

export default PurchaseReceiptCommercial;
