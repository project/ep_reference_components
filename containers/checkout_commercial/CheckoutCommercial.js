/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import ReactRouterPropTypes from 'react-router-prop-types';
import intl from 'react-intl-universal';
import Config from 'config';
import { login } from '../../common/AuthService';
import CheckoutSummaryCommercial from '../../components/checkout_summary_commercial/checkout.summary.commercial';
import AddressFormCommercialMain from '../../components/address_form_commercial/addressformcommercial.main';
import PaymentFormCommercialMain from '../../components/payment_form_commercial/paymentformcommercial.main';
import PaymentMethodContainer from '../../components/paymentmethod_container/paymentmethod.container';
import { cortexFetch } from '../../common/Cortex';

// Array of zoom parameters to pass to Cortex
const zoomArray = [
  // zooms for checkout summary
  'defaultcart',
  'defaultcart:total',
  'defaultcart:discount',
  'defaultcart:order',
  'defaultcart:order:tax',
  'defaultcart:order:total',
  'defaultcart:appliedpromotions:element',
  'defaultcart:order:couponinfo:coupon',
  'defaultcart:order:couponinfo:couponform',
  // zooms for billing address
  'defaultcart:order:billingaddressinfo:billingaddress',
  'defaultcart:order:billingaddressinfo:selector:choice',
  'defaultcart:order:billingaddressinfo:selector:choice:description',
  // zooms for shipping address
  'defaultcart:order:deliveries:element:destinationinfo:destination',
  'defaultcart:order:deliveries:element:destinationinfo:selector:choice',
  'defaultcart:order:deliveries:element:destinationinfo:selector:choice:description',
  // zooms for shipping options
  'defaultcart:order:deliveries:element:shippingoptioninfo:shippingoption',
  'defaultcart:order:deliveries:element:shippingoptioninfo:selector:choice',
  'defaultcart:order:deliveries:element:shippingoptioninfo:selector:choice:description',
  // zooms for payment methods
  'defaultcart:order:paymentmethodinfo:paymentmethod',
  'defaultcart:order:paymentmethodinfo:selector:choice',
  'defaultcart:order:paymentmethodinfo:selector:choice:description',
  'defaultcart:order:paymentmethodinfo:paymenttokenform',
  // zooms for comment form
  'defaultcart:order:commentform',
];

class CheckoutCommercial extends React.Component {
  static propTypes = {
    history: ReactRouterPropTypes.history.isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {
      orderData: undefined,
      addressData: undefined,
      isLoading: false,
      accordianFocus: 'billingShipping',
      paymentType: undefined,
      poName: undefined,
      poNumber: undefined,
      comment: undefined,
    };
  }

  componentDidMount() {
    this.fetchOrderData();
  }

  fetchOrderData() {
    login().then(() => {
      cortexFetch(`/?zoom=${zoomArray.sort().join()}`,
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
          },
        })
        .then(res => res.json())
        .then((res) => {
          const paymentMethod = res._defaultcart[0]._order[0]._paymentmethodinfo[0]._paymentmethod;
          let poName;
          let poNumber;
          if (paymentMethod && paymentMethod[0]['display-name'].startsWith('Reference:')) {
            poName = paymentMethod[0]['display-name'].replace('Reference: ', '');
            poNumber = paymentMethod[0].token;
          }
          this.setState({
            orderData: res._defaultcart[0],
            isLoading: false,
            poName,
            poNumber,
          });
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error.message);
        });
    });
  }

  updateDefaultBilling() {
    login().then(() => {
      cortexFetch(`/?zoom=${zoomArray.sort().join()}`,
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
          },
        })
        .then(res => res.json())
        .then((res) => {
          const billingAddressInfo = res._defaultcart[0]._order[0]._billingaddressinfo;
          if (billingAddressInfo && !billingAddressInfo[0]._billingaddress) {
            const selector = res._defaultcart[0]._order[0]._billingaddressinfo[0]._selector;
            if (selector) {
              const choices = selector[0]._choice;
              if (choices.length === 1) {
                this.handleChange(choices[0].links.find(link => link.rel === 'selectaction').uri);
              }
            }
          }
          this.setState({
            orderData: res._defaultcart[0],
            isLoading: false,
          });
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error.message);
        });
    });
  }

  editAddress(addressLink) {
    this.setState({
      addressData: addressLink,
    });
  }

  closeAddressForm() {
    this.setState({
      addressData: undefined,
    });
    this.updateDefaultBilling();
  }

  reviewOrder() {
    const { history } = this.props;
    history.push('/order');
    window.location.reload();
  }

  handleComment() {
    const { comment, orderData } = this.state;
    const commentForm = orderData._order[0]._commentform;

    if (commentForm && commentForm[0].links[0].uri && comment) {
      this.setState({
        isLoading: true,
      });
      login().then(() => {
        cortexFetch(commentForm[0].links[0].uri, {
          method: 'post',
          headers: {
            'Content-Type': 'application/json',
            Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
          },
          body: JSON.stringify({
            // eslint-disable-next-line react/destructuring-assignment
            comment,
          }),
        }).then(() => {
          this.reviewOrder();
        }).catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error.message);
        });
      });
    }

    this.reviewOrder();
  }

  updateComment(event) {
    this.setState({ comment: event.target.value });
  }

  handlePONumber(link) {
    this.setState({
      isLoading: true,
    });
    login().then(() => {
      cortexFetch(link, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
        },
        body: JSON.stringify({
          // eslint-disable-next-line react/destructuring-assignment
          'display-name': `Reference: ${this.state.poName}`,
          // eslint-disable-next-line react/destructuring-assignment
          token: this.state.poNumber,
        }),
      }).then(() => {
        this.fetchOrderData();
      }).catch((error) => {
        // eslint-disable-next-line no-console
        console.error(error.message);
      });
    });
  }

  updatePOName(event) {
    this.setState({ poName: event.target.value });
  }

  updatePONumber(event) {
    this.setState({ poNumber: event.target.value });
  }

  disablePOContinue() {
    const { orderData, paymentType } = this.state;
    if (this.needPayment()) {
      return true;
    }
    const paymentMethod = orderData._order[0]._paymentmethodinfo[0]._paymentmethod;
    return !(paymentType === 'po' && paymentMethod && paymentMethod[0]['display-name'].startsWith('Reference:'));
  }

  disableAdditionalInfo() {
    const { orderData, paymentType } = this.state;
    if (this.needPayment() || this.needAddress() || paymentType === undefined) {
      return true;
    }
    if (paymentType === 'credit') {
      return false;
    }
    const paymentMethod = orderData._order[0]._paymentmethodinfo[0]._paymentmethod;
    return !(paymentType === 'po' && paymentMethod && paymentMethod[0]['display-name'].startsWith('Reference:'));
  }

  handleDelete(link) {
    this.setState({
      isLoading: true,
    });
    login().then(() => {
      cortexFetch(link, {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
        },
      }).then(() => {
        this.fetchOrderData();
      }).catch((error) => {
        // eslint-disable-next-line no-console
        console.error(error.message);
      });
    });
  }

  handleChange(link) {
    this.setState({
      isLoading: true,
    });
    login().then(() => {
      cortexFetch(link, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
        },
      }).then(() => {
        this.fetchOrderData();
      }).catch((error) => {
        // eslint-disable-next-line no-console
        console.error(error.message);
      });
    });
  }

  handleChangeFocus(id) {
    const { paymentType } = this.state;
    if (id === 'paymentInfo' && paymentType === undefined) {
      this.setState({
        paymentType: 'po',
      });
    }
    this.setState({
      accordianFocus: id,
    });
  }

  handlePaymentType(event) {
    this.setState({
      paymentType: event.target.value,
    });
    if (event.target.value === 'credit') {
      const { orderData } = this.state;
      const selector = orderData._order[0]._paymentmethodinfo[0]._selector;
      if (selector) {
        const choice = selector[0]._choice[0];
        this.handleChange(choice.links.find(link => link.rel === 'selectaction').uri);
      }
    }
  }

  handlePaymentContinue() {
    const { paymentType } = this.state;

    if (paymentType === 'po') {
      this.handleChangeFocus('poInfo');
    } else {
      this.handleChangeFocus('additionalInfo');
    }
  }

  needAddress() {
    const { orderData } = this.state;
    const { messages } = orderData._order[0];
    if (messages.find(message => message.id === 'need.shipping.address') || messages.find(message => message.id === 'need.billing.address')) {
      return true;
    }
    return false;
  }

  needPayment() {
    const { orderData } = this.state;
    const { messages } = orderData._order[0];
    if (messages.find(message => message.id === 'need.payment.method')) {
      return true;
    }
    return false;
  }

  renderBillingAddress() {
    const { orderData } = this.state;
    if (orderData._order[0]._billingaddressinfo) {
      const billingAddresses = [];
      const billingAddress = orderData._order[0]._billingaddressinfo[0]._billingaddress;
      if (billingAddress) {
        const [description] = billingAddress;
        description.checked = true;
        billingAddresses.push(description);
      }
      const selector = orderData._order[0]._billingaddressinfo[0]._selector;
      if (selector) {
        const choices = selector[0]._choice;
        choices.map((choice) => {
          const [description] = choice._description;
          description.selectaction = choice.links.find(link => link.rel === 'selectaction').uri;
          description.checked = false;
          billingAddresses.push(description);
          return description;
        });
      }
      return (
        billingAddresses.map((billingAddr) => {
          const {
            name, address, selectaction, checked,
          } = billingAddr;
          return (
            <div key={`billingAddress_${Math.random().toString(36).substr(2, 9)}`} className="row address-row">
              <div className="col-sm-8 col-6">
                <div className="row">
                  <div className="col-sm-6">
                    <input type="radio" name="billing" id="billingOption" className="form-check-input" defaultChecked={checked} onChange={() => this.handleChange(selectaction)} />
                    <label htmlFor="billingOption" className="form-check-label name-label">
                      <div>
                        {name['given-name']}
                        &nbsp;
                        {name['family-name']}
                      </div>
                    </label>
                  </div>
                  <ul className="col-sm-6">
                    <li className="address-street-address">
                      {address['street-address']}
                    </li>
                    <li className="address-extended-address">
                      {address['extended-address']}
                    </li>
                    <li className="address-geographical">
                      <span className="address-city">
                        {address.locality}
                        ,&nbsp;
                      </span>
                      <span className="address-region">
                        {address.region}
                        ,&nbsp;
                      </span>
                      <span className="address-country">
                        {address['country-name']}
                        &nbsp;
                      </span>
                      <span className="address-postal-code">
                        {address['postal-code']}
                      </span>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-sm-4 col-6 text-right">
                {/* eslint-disable-next-line max-len */}
                <button className="ep-btn small checkout-edit-address-btn" type="button" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#addressModal" onClick={() => { this.editAddress(billingAddr.self.uri); }}>
                  {intl.get('edit')}
                </button>
                {/* eslint-disable-next-line max-len */}
                <button className="ep-btn small checkout-delete-address-btn" type="button" onClick={() => { this.handleDelete(billingAddr.self.uri); }}>
                  {intl.get('delete')}
                </button>
              </div>
            </div>
          );
        })
      );
    }
    return (
      <div>
        <p>
          {intl.get('no-billing-address-message')}
        </p>
      </div>
    );
  }

  renderBillingAddressSelector() {
    return (
      <div>
        <h2>
          {intl.get('billing-address')}
        </h2>
        <div data-region="billingAddressSelectorsRegion" className="checkout-region-inner-container">
          {this.renderBillingAddress()}
        </div>
        <button className="ep-btn primary wide checkout-new-address-btn" type="button" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#addressModal">
          {intl.get('add-new-address')}
        </button>
      </div>
    );
  }

  renderShippingAddress() {
    const { orderData } = this.state;
    if (orderData._order[0]._deliveries && orderData._order[0]._deliveries[0]._element[0]._destinationinfo) {
      const shippingAddresses = [];
      const destination = orderData._order[0]._deliveries[0]._element[0]._destinationinfo[0]._destination;
      if (destination) {
        const [description] = destination;
        description.checked = true;
        shippingAddresses.push(description);
      }
      const selector = orderData._order[0]._deliveries[0]._element[0]._destinationinfo[0]._selector;
      if (selector) {
        const choices = selector[0]._choice;
        choices.map((choice) => {
          const [description] = choice._description;
          description.selectaction = choice.links.find(link => link.rel === 'selectaction').uri;
          description.checked = false;
          shippingAddresses.push(description);
          return description;
        });
      }
      return (
        shippingAddresses.map((shippingAddress) => {
          const {
            name, address, selectaction, checked,
          } = shippingAddress;
          return (
            <div key={`shippingAddress_${Math.random().toString(36).substr(2, 9)}`} className="row address-row">
              <div className="col-sm-8 col-6">
                <div className="row">
                  <div className="col-sm-6">
                    <input type="radio" name="shippingAddress" id="shippingAddress" className="form-check-input" defaultChecked={checked} onChange={() => this.handleChange(selectaction)} />
                    <label htmlFor="shippingAddress" className="form-check-label name-label">
                      <div>
                        {name['given-name']}
                        &nbsp;
                        {name['family-name']}
                      </div>
                    </label>
                  </div>
                  <ul className="col-sm-6">
                    <li className="address-street-address">
                      {address['street-address']}
                    </li>
                    <li className="address-extended-address">
                      {address['extended-address']}
                    </li>
                    <li className="address-geographical">
                      <span className="address-city">
                        {address.locality}
                        ,&nbsp;
                      </span>
                      <span className="address-region">
                        {address.region}
                        ,&nbsp;
                      </span>
                      <span className="address-country">
                        {address['country-name']}
                        &nbsp;
                      </span>
                      <span className="address-postal-code">
                        {address['postal-code']}
                      </span>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-sm-4 col-6 text-right">
                {/* eslint-disable-next-line max-len */}
                <button className="ep-btn small checkout-edit-address-btn" type="button" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#addressModal" onClick={() => { this.editAddress(shippingAddress.self.uri); }}>
                  {intl.get('edit')}
                </button>
                {/* eslint-disable-next-line max-len */}
                <button className="ep-btn small checkout-delete-address-btn" type="button" onClick={() => { this.handleDelete(shippingAddress.self.uri); }}>
                  {intl.get('delete')}
                </button>
              </div>
            </div>
          );
        })
      );
    }
    return (
      <div>
        <p>
          {intl.get('no-shipping-address-message')}
        </p>
      </div>
    );
  }

  renderShippingAddressSelector() {
    const { orderData } = this.state;
    const deliveries = orderData._order[0]._deliveries;
    const { messages } = orderData._order[0];
    const needShipmentDetails = messages.find(message => message.id === 'need.shipping.address');
    if (needShipmentDetails || deliveries) {
      return (
        <div data-region="shippingAddressesRegion" style={{ display: 'block' }}>
          <div>
            <h2>
              {intl.get('shipping-address')}
            </h2>
            <div data-region="shippingAddressSelectorsRegion" className="checkout-region-inner-container">
              {this.renderShippingAddress()}
            </div>
            <button className="ep-btn primary wide checkout-new-address-btn" type="button" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#addressModal">
              {intl.get('add-new-address')}
            </button>
          </div>
        </div>
      );
    }
    return null;
  }

  renderShippingOptions() {
    const { orderData } = this.state;
    if (orderData._order[0]._deliveries && orderData._order[0]._deliveries[0]._element[0]._shippingoptioninfo) {
      const shippingOptions = [];
      const shippingOption = orderData._order[0]._deliveries[0]._element[0]._shippingoptioninfo[0]._shippingoption;
      if (shippingOption) {
        const [description] = shippingOption;
        description.checked = true;
        shippingOptions.push(description);
      }
      const selector = orderData._order[0]._deliveries[0]._element[0]._shippingoptioninfo[0]._selector;
      if (selector && selector[0]._choice) {
        const choices = selector[0]._choice;
        choices.map((choice) => {
          const [description] = choice._description;
          description.selectaction = choice.links.find(link => link.rel === 'selectaction').uri;
          description.checked = false;
          shippingOptions.push(description);
          return description;
        });
      }
      return (
        shippingOptions.map(option => (
          <div key={`shippingOption_${Math.random().toString(36).substr(2, 9)}`} className="address-row row">
            <div className="col-8 col-md-9 col-lg-4">
              <input type="radio" name="shippingOption" id="shippingOption" className="form-check-input" defaultChecked={option.checked} onChange={() => this.handleChange(option.selectaction)} />
              <label htmlFor="shippingOption" className="form-check-label name-label">
                {option['display-name']}
              </label>
              <div className="shippingOptionCarrier name-label">
                {option.carrier}
              </div>
            </div>
            <div className="shippingOptionCost col-4 col-md-3 col-lg-4">
              {option.cost[0].display}
            </div>
          </div>
        ))
      );
    }
    return (
      <div className="no-shipping">
        <p>
          {intl.get('no-shipping-options-message')}
        </p>
      </div>
    );
  }

  renderShippingOptionsSelector() {
    const { orderData } = this.state;
    const deliveries = orderData._order[0]._deliveries;
    if (deliveries && deliveries[0]._element[0]._destinationinfo) {
      return (
        <div>
          <h2>
            {intl.get('shipping-option')}
          </h2>
          <div data-region="shippingOptionSelectorsRegion">
            {this.renderShippingOptions()}
          </div>
        </div>
      );
    }
    return null;
  }

  renderBillingShipping() {
    const { orderData, accordianFocus, addressData } = this.state;

    if (accordianFocus === 'billingShipping') {
      return (
        <div id="billingShipping" className={`collapse ${accordianFocus === 'billingShipping' ? 'show' : ''} collapse-content`} data-parent="#accordion">
          <div className="billing-container" data-region="billingAddressesRegion">
            {this.renderBillingAddressSelector()}
          </div>
          <hr className="divider" />
          <div className="shipping-container">
            {this.renderShippingAddressSelector()}
          </div>
          <hr className="divider" />
          {this.renderShippingOptionsSelector()}
          <AddressFormCommercialMain addressData={addressData} saveAddress={() => { this.closeAddressForm(); }} />
        </div>
      );
    }
    const shippingAddress = orderData._order[0]._deliveries[0]._element[0]._destinationinfo[0]._destination;
    const billingAddress = orderData._order[0]._billingaddressinfo[0]._billingaddress;
    if (billingAddress[0] && shippingAddress[0]) {
      return (
        <div className="collapsed-content container">
          <div className="row collapsed-row">
            <div className="col-12 collapsed-label">
              <h2>
                {intl.get('billing-address')}
              </h2>
            </div>
            <div className="col-6">
              {billingAddress[0].name['given-name']}
              &nbsp;
              {billingAddress[0].name['family-name']}
            </div>
            <ul className="col-6">
              <li className="address-street-address">
                {billingAddress[0].address['street-address']}
              </li>
              <li className="address-extended-address">
                {billingAddress[0].address['extended-address']}
              </li>
              <li className="address-geographical">
                <span className="address-city">
                  {billingAddress[0].address.locality}
                  ,&nbsp;
                </span>
                <span className="address-region">
                  {billingAddress[0].address.region}
                  ,&nbsp;
                </span>
                <span className="address-country">
                  {billingAddress[0].address['country-name']}
                  &nbsp;
                </span>
                <span className="address-postal-code">
                  {billingAddress[0].address['postal-code']}
                </span>
              </li>
            </ul>
          </div>
          <div className="row collapsed-row">
            <div className="col-12 collapsed-label">
              <h2>
                {intl.get('shipping-address')}
              </h2>
            </div>
            <div className="col-6">
              {shippingAddress[0].name['given-name']}
              &nbsp;
              {shippingAddress[0].name['family-name']}
            </div>
            <ul className="col-6">
              <li className="address-street-address">
                {shippingAddress[0].address['street-address']}
              </li>
              <li className="address-extended-address">
                {shippingAddress[0].address['extended-address']}
              </li>
              <li className="address-geographical">
                <span className="address-city">
                  {shippingAddress[0].address.locality}
                  ,&nbsp;
                </span>
                <span className="address-region">
                  {shippingAddress[0].address.region}
                  ,&nbsp;
                </span>
                <span className="address-country">
                  {shippingAddress[0].address['country-name']}
                  &nbsp;
                </span>
                <span className="address-postal-code">
                  {shippingAddress[0].address['postal-code']}
                </span>
              </li>
            </ul>
          </div>
        </div>
      );
    }
    return null;
  }

  renderPayments() {
    const { orderData } = this.state;
    if (orderData._order[0]._paymentmethodinfo) {
      const paymentMethods = [];
      const paymentMethod = orderData._order[0]._paymentmethodinfo[0]._paymentmethod;
      if (paymentMethod) {
        const [description] = paymentMethod;
        description.checked = true;
        description.deletable = false;
        paymentMethods.push(description);
      }
      const selector = orderData._order[0]._paymentmethodinfo[0]._selector;
      if (selector) {
        const choices = selector[0]._choice;
        choices.map((choice) => {
          const [description] = choice._description;
          description.selectaction = choice.links.find(link => link.rel === 'selectaction').uri;
          description.checked = false;
          description.deletable = true;
          paymentMethods.push(description);
          return description;
        });
      }
      return (
        paymentMethods.map((payment) => {
          const {
            checked, deletable, selectaction,
          } = payment;
          const displayName = payment['display-name'];
          return (
            <div key={`paymentMethod_${Math.random().toString(36).substr(2, 9)}`}>
              <div className="row payment-row">
                <div className="col-sm-6" data-region="paymentSelector">
                  <label htmlFor="paymentMethod">
                    <input type="radio" name="paymentMethod" id="paymentMethod" className="payment-option-radio" defaultChecked={checked} onChange={() => this.handleChange(selectaction)} />
                    <div className="paymentMethodComponentRegion" data-region="paymentMethodComponentRegion">
                      <PaymentMethodContainer displayName={displayName} />
                    </div>
                  </label>
                </div>
                {deletable && (
                  <div className="col-sm-6 text-right">
                    <button className="ep-btn small checkout-delete-payment-btn" type="button" onClick={() => { this.handleDelete(payment.self.uri); }}>
                      {intl.get('delete')}
                    </button>
                  </div>
                )}
              </div>
            </div>
          );
        })
      );
    }
    return (
      <div>
        <p>
          {intl.get('no-saved-payment-method-message')}
        </p>
      </div>
    );
  }

  renderPaymentSelector() {
    const { paymentType } = this.state;
    return (
      <div>
        <h2 className="payment-title">
          {intl.get('payment-method')}
        </h2>
        <div className="form-check-inline payment-radio-group">
          <label className="form-check-label" htmlFor="poRadio">
            <input id="poRadio" type="radio" className="form-check-input" value="po" onChange={(event) => { this.handlePaymentType(event); }} checked={paymentType === 'po'} />
            {intl.get('terms')}
          </label>
          <label className="form-check-label radio-right" htmlFor="creditRadio">
            <input id="creditRadio" type="radio" className="form-check-input" value="credit" onChange={(event) => { this.handlePaymentType(event); }} checked={paymentType === 'credit'} />
            {intl.get('credit-card')}
          </label>
        </div>
        <div className="credit-container" hidden={paymentType !== 'credit'}>
          <div data-region="paymentMethodSelectorsRegion" className="checkout-region-inner-container">
            {this.renderPayments()}
          </div>
          <button className="ep-btn primary wide checkout-new-payment-btn" type="button" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#paymentModal">
            {intl.get('add-new-payment-method')}
          </button>
        </div>
      </div>
    );
  }

  renderPaymentType() {
    const {
      orderData, accordianFocus, paymentType,
    } = this.state;
    if (accordianFocus === 'paymentInfo') {
      return (
        <div id="paymentInfo" className={`collapse ${accordianFocus === 'paymentInfo' ? 'show' : ''} collapse-content`} data-parent="#accordion">
          {this.renderPaymentSelector()}
          <PaymentFormCommercialMain savePayment={() => { this.fetchOrderData(); }} />
        </div>
      );
    }
    if (orderData._order[0]._paymentmethodinfo && !this.needPayment()) {
      const paymentMethod = orderData._order[0]._paymentmethodinfo[0]._paymentmethod;
      if (paymentMethod && paymentType === 'credit') {
        return (
          <div className="container">
            {intl.get('payment-method')}
            :&nbsp;
            {paymentMethod[0]['display-name']}
          </div>
        );
      }
      if (paymentType === 'po') {
        return (
          <div className="container">
            {intl.get('payment-method')}
            :&nbsp;
            {intl.get('terms')}
          </div>
        );
      }
    }
    return null;
  }

  renderPO() {
    const {
      orderData, accordianFocus, paymentType, poName, poNumber,
    } = this.state;
    if (accordianFocus === 'poInfo') {
      if (orderData._order[0]._paymentmethodinfo[0]._paymenttokenform) {
        const tokenFormUrl = orderData._order[0]._paymentmethodinfo[0]._paymenttokenform[0].links[0].uri;
        return (
          <div id="poInfo" className={`collapse ${accordianFocus === 'poInfo' ? 'show' : ''} collapse-content`} data-parent="#accordion">
            <form>
              <div className="form-group row">
                {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                <label htmlFor="poName" className="col-4 col-sm-2">
                  {intl.get('reference')}
                </label>
                <div className="col-8 col-sm-4">
                  <input type="text" className="form-control" id="poName" value={poName} onChange={(event) => { this.updatePOName(event); }} />
                </div>
              </div>
              <div className="form-group row">
                {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                <label htmlFor="poNumber" className="col-4 col-sm-2">
                  {intl.get('po')}
                </label>
                <div className="col-8 col-sm-4">
                  <input type="text" className="form-control" id="poNumber" value={poNumber} onChange={(event) => { this.updatePONumber(event); }} />
                </div>
              </div>
              <button className="ep-btn primary wide checkout-new-payment-btn" type="button" onClick={() => { this.handlePONumber(tokenFormUrl); }}>
                { intl.get('add-purchase-order') }
              </button>
            </form>
          </div>
        );
      }
    }
    const paymentMethod = orderData._order[0]._paymentmethodinfo[0]._paymentmethod;
    if (paymentType === 'po' && paymentMethod && paymentMethod[0]['display-name'].startsWith('Reference:')) {
      return (
        <div className="container">
          {intl.get('po')}
          :&nbsp;
          {paymentMethod[0].token}
        </div>
      );
    }
    return null;
  }

  renderAdditionalInfo() {
    const { accordianFocus, comment } = this.state;
    if (accordianFocus === 'additionalInfo') {
      return (
        <div id="additionalInfo" className={`collapse ${accordianFocus === 'additionalInfo' ? 'show' : ''} collapse-content`} data-parent="#accordion">
          <h2>
            {intl.get('comments')}
          </h2>
          <textarea className="comment-area" value={comment} onChange={(event) => { this.updateComment(event); }} />
        </div>
      );
    }
    return null;
  }

  render() {
    const {
      orderData, isLoading, accordianFocus, paymentType,
    } = this.state;
    if (orderData && !isLoading) {
      const { messages } = orderData._order[0];
      return (
        <div className="row">
          <div id="accordion" className="checkout-commercial-container col-md-9">
            <h2 className="view-title">
              {intl.get('checkout-title')}
            </h2>
            <div className="checkout-commercial-block">
              <div className="billing-shipping-container checkout-commercial-component">
                <div className={`checkout-commercial-header ${accordianFocus === 'billingShipping' ? 'focused-header' : ''}`}>
                  <h2 className="header-title">
                    {intl.get('billing-shipping')}
                  </h2>
                  <button
                    className="btn"
                    type="button"
                    data-toggle="collapse"
                    data-target="#billingShipping"
                    aria-controls="billingShipping"
                    aria-expanded="true"
                    onClick={() => { this.handleChangeFocus('billingShipping'); }}
                    hidden={accordianFocus === 'billingShipping'}
                  >
                    {intl.get('edit')}
                  </button>
                </div>
                {this.renderBillingShipping()}
              </div>
              <div className="text-right">
                <button className="ep-btn primary" type="button" disabled={this.needAddress()} hidden={accordianFocus !== 'billingShipping'} onClick={() => { this.handleChangeFocus('paymentInfo'); }}>
                  {intl.get('continue')}
                </button>
              </div>
            </div>
            <div className="checkout-commercial-block">
              <div className="payment-container checkout-commercial-component">
                <div className={`checkout-commercial-header ${accordianFocus === 'paymentInfo' ? 'focused-header' : ''}`}>
                  <h2 className="header-title">
                    {intl.get('payment-info')}
                  </h2>
                  <button
                    className="btn"
                    type="button"
                    data-toggle="collapse"
                    data-target="#paymentInfo"
                    aria-controls="paymentInfo"
                    aria-expanded="false"
                    onClick={() => { this.handleChangeFocus('paymentInfo'); }}
                    hidden={accordianFocus === 'paymentInfo' || this.needAddress()}
                  >
                    {intl.get('edit')}
                  </button>
                </div>
                {this.renderPaymentType()}
              </div>
              <div className="text-right">
                <button className="ep-btn primary" type="button" disabled={this.needPayment() && paymentType === 'credit'} hidden={accordianFocus !== 'paymentInfo'} onClick={() => { this.handlePaymentContinue(); }}>
                  {intl.get('continue')}
                </button>
              </div>
            </div>
            <div className="checkout-commercial-block">
              <div className="po-container checkout-commercial-component">
                <div className={`checkout-commercial-header ${accordianFocus === 'poInfo' ? 'focused-header' : ''}`}>
                  <h2 className="header-title">
                    {intl.get('term-info')}
                  </h2>
                  <button
                    className="btn"
                    type="button"
                    data-toggle="collapse"
                    data-target="#poInfo"
                    aria-controls="poInfo"
                    aria-expanded="false"
                    onClick={() => { this.handleChangeFocus('poInfo'); }}
                    hidden={accordianFocus === 'poInfo' || paymentType !== 'po'}
                  >
                    {intl.get('edit')}
                  </button>
                </div>
                {this.renderPO()}
              </div>
              <div className="text-right">
                <button className="ep-btn primary" type="button" disabled={this.disablePOContinue()} hidden={accordianFocus !== 'poInfo'} onClick={() => { this.handleChangeFocus('additionalInfo'); }}>
                  {intl.get('continue')}
                </button>
              </div>
            </div>
            <div className="checkout-commercial-block">
              <div className="additional-container checkout-commercial-component">
                <div className={`checkout-commercial-header ${accordianFocus === 'additionalInfo' ? 'focused-header' : ''}`}>
                  <h2 className="header-title">
                    {intl.get('additional-info')}
                  </h2>
                  <button
                    className="btn"
                    type="button"
                    data-toggle="collapse"
                    data-target="#additionalInfo"
                    aria-controls="additionalInfo"
                    aria-expanded="false"
                    onClick={() => { this.handleChangeFocus('additionalInfo'); }}
                    hidden={accordianFocus === 'additionalInfo' || this.disableAdditionalInfo()}
                  >
                    {intl.get('edit')}
                  </button>
                </div>
                {this.renderAdditionalInfo()}
              </div>
              <div className="text-right">
                <button className="ep-btn primary" type="button" hidden={accordianFocus !== 'additionalInfo'} disabled={messages[0]} onClick={() => { this.handleComment(); }}>
                  {intl.get('place-order')}
                </button>
              </div>
            </div>
          </div>
          <div className="checkout-commercial-container col-md-3">
            <h2 className="view-title">
              {intl.get('order-details')}
            </h2>
            <CheckoutSummaryCommercial data={orderData} />
          </div>
        </div>
      );
    }
    return (
      <div className="checkout-container container">
        <div className="checkout-container-inner">
          <div data-region="checkoutTitleRegion" className="checkout-title-container" style={{ display: 'block' }}>
            <div>
              <h1 className="view-title">
                {intl.get('checkout-summary')}
              </h1>
            </div>
          </div>
          <div className="checkout-main-container">
            <div className="loader" />
          </div>
        </div>
      </div>
    );
  }
}

export default CheckoutCommercial;
