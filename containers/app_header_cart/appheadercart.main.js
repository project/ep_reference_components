/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import Config from 'config';
import { cortexFetch } from '../../common/Cortex';
import { login } from '../../common/AuthService';

// Array of zoom parameters to pass to Cortex
const zoomArray = [
  'defaultcart',
];

class AppHeaderCart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cartData: undefined,
      isLoading: true,
    };
  }

  componentDidMount() {
    this.fetchCartData();
  }

  componentWillReceiveProps() {
    this.fetchCartData();
  }

  fetchCartData() {
    login().then(() => {
      cortexFetch(`/?zoom=${zoomArray.sort().join()}`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
        },
      })
        .then(res => res.json())
        .then((res) => {
          this.setState({
            cartData: res._defaultcart[0],
            isLoading: false,
          });
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error.message);
        });
    });
  }

  render() {
    const {
      cartData,
      isLoading,
    } = this.state;

    return [
      <div className="cart-link-container">
        <a className="cart-link" href="/mybag">
          {cartData && cartData['total-quantity'] !== 0 && !isLoading && (
            <span className="cart-link-counter">
              {cartData && cartData['total-quantity']}
            </span>
          )}
        </a>
      </div>,
    ];
  }
}

export default AppHeaderCart;
