/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import intl from 'react-intl-universal';
import Config from 'config';
import { login } from '../../common/AuthService';
import PaymentMethodContainer from '../../components/paymentmethod_container/paymentmethod.container';
import { cortexFetch } from '../../common/Cortex';

// Array of zoom parameters to pass to Cortex
const zoomArray = [
  // zooms for payment methods
  'defaultcart:order:paymentmethodinfo:paymentmethod',
  'defaultcart:order:paymentmethodinfo:selector:choice',
  'defaultcart:order:paymentmethodinfo:selector:choice:description',
  'defaultcart:order:paymentmethodinfo:paymenttokenform',
];

class PayByPO extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orderData: undefined,
      isLoading: false,
    };
  }

  componentDidMount() {
    this.fetchOrderData();
  }

  fetchOrderData() {
    login().then(() => {
      cortexFetch(`/?zoom=${zoomArray.sort().join()}`,
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
          },
        })
        .then(res => res.json())
        .then((res) => {
          this.setState({
            orderData: res._defaultcart[0],
            isLoading: false,
          });
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error.message);
        });
    });
  }

  handleDelete(link) {
    this.setState({
      isLoading: true,
    });
    login().then(() => {
      cortexFetch(link, {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
        },
      }).then(() => {
        this.fetchOrderData();
      }).catch((error) => {
        // eslint-disable-next-line no-console
        console.error(error.message);
      });
    });
  }

  handleChange(link) {
    this.setState({
      isLoading: true,
    });
    login().then(() => {
      cortexFetch(link, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
        },
      }).then(() => {
        this.fetchOrderData();
      }).catch((error) => {
        // eslint-disable-next-line no-console
        console.error(error.message);
      });
    });
  }

  handlePONumber(link) {
    this.setState({
      isLoading: true,
    });
    login().then(() => {
      cortexFetch(link, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
        },
        body: JSON.stringify({
          // eslint-disable-next-line react/destructuring-assignment
          'display-name': `PO: ${this.state.poNumber}`,
          // eslint-disable-next-line react/destructuring-assignment
          token: this.state.poNumber,
        }),
      }).then(() => {
        this.fetchOrderData();
      }).catch((error) => {
        // eslint-disable-next-line no-console
        console.error(error.message);
      });
    });
  }

  updatePONumber(event) {
    this.setState({ poNumber: event.target.value });
  }

  renderPayments() {
    const { orderData } = this.state;
    if (orderData._order[0]._paymentmethodinfo) {
      const paymentMethods = [];
      const paymentMethod = orderData._order[0]._paymentmethodinfo[0]._paymentmethod;
      if (paymentMethod) {
        const [description] = paymentMethod;
        description.checked = true;
        description.deletable = false;
        paymentMethods.push(description);
      }
      const selector = orderData._order[0]._paymentmethodinfo[0]._selector;
      if (selector) {
        const choices = selector[0]._choice;
        choices.map((choice) => {
          const [description] = choice._description;
          description.selectaction = choice.links.find(link => link.rel === 'selectaction').uri;
          description.checked = false;
          description.deletable = true;
          paymentMethods.push(description);
          return description;
        });
      }
      return (
        paymentMethods.map((payment) => {
          const {
            checked, deletable, selectaction,
          } = payment;
          const displayName = payment['display-name'];
          return (
            <div key={`paymentMethod_${Math.random().toString(36).substr(2, 9)}`}>
              <div className="payment-ctrl-cell" data-region="paymentSelector">
                <label htmlFor="paymentMethod">
                  <input type="radio" name="paymentMethod" id="paymentMethod" className="payment-option-radio" defaultChecked={checked} onChange={() => this.handleChange(selectaction)} />
                  <div className="paymentMethodComponentRegion" data-region="paymentMethodComponentRegion" style={{ display: 'block' }}>
                    <PaymentMethodContainer displayName={displayName} />
                  </div>
                </label>
              </div>
              {deletable && (
                <div className="payment-btn-cell">
                  <button className="ep-btn small checkout-delete-payment-btn" type="button" onClick={() => { this.handleDelete(payment.self.uri); }}>
                    {intl.get('delete')}
                  </button>
                </div>
              )}
            </div>
          );
        })
      );
    }
    return (
      <div>
        <p>
          {intl.get('no-saved-payment-method-message')}
        </p>
      </div>
    );
  }

  renderPaymentSelector() {
    const { orderData } = this.state;
    if (orderData._order[0]._paymentmethodinfo[0]._paymenttokenform) {
      const tokenFormUrl = orderData._order[0]._paymentmethodinfo[0]._paymenttokenform[0].links[0].uri;
      return (
        <div>
          <div data-region="paymentMethodSelectorsRegion" className="checkout-region-inner-container">
            {this.renderPayments()}
          </div>
          <div className="form-input">
            <input type="text" className="form-control pay-by-po-input" name="po-input" id="po-input" placeholder={intl.get('input-po')} onChange={(event) => { this.updatePONumber(event); }} />
          </div>
          <button className="ep-btn primary wide checkout-new-payment-btn" type="button" onClick={() => { this.handlePONumber(tokenFormUrl); }}>
            { intl.get('add-purchase-order') }
          </button>
        </div>
      );
    }
    return (
      <div>
        <div data-region="paymentMethodSelectorsRegion" className="checkout-region-inner-container">
          {this.renderPayments()}
        </div>
        <button className="ep-btn primary wide checkout-new-payment-btn" type="button" onClick={() => { this.newPayment(); }}>
          {intl.get('add-new-payment-method')}
        </button>
      </div>
    );
  }

  render() {
    const { orderData, isLoading } = this.state;
    if (orderData && !isLoading) {
      return (
        <div className="payment-methods-container container">
          <div className="payment-methods-inner">
            <div data-region="checkoutTitleRegion" className="checkout-title-container" style={{ display: 'block' }}>
              <div>
                <h1 className="view-title">
                  {intl.get('payment-method')}
                </h1>
              </div>
            </div>
            <div className="payment-methods-main-container">
              <div data-region="paymentMethodsRegion" style={{ display: 'block' }}>
                {this.renderPaymentSelector()}
              </div>
            </div>
          </div>
        </div>
      );
    }
    return (
      <div className="payment-methods-container">
        <div className="loader" />
      </div>
    );
  }
}

export default PayByPO;
