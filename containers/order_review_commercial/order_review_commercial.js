/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router, Switch, Route,
} from 'react-router-dom';
import intl from 'react-intl-universal';
import OrderReviewCommercial from './OrderReviewCommercial';
import onComponentLoad from '../../common/onComponentLoad';
import locales from '../../common/locales';
import { getSelectedLocaleValue } from '../../common/UserPrefs';

const OrderReviewCommercialComponent = () => (
  <Switch>
    <Route exact path="/*" component={OrderReviewCommercial} />
  </Switch>
);

const OrderReviewCommercialComponentWithRouter = () => (
  <Router>
    <OrderReviewCommercialComponent />
  </Router>
);

onComponentLoad('ep-order-review-commercial', (elementId) => {
  intl.init({
    currentLocale: getSelectedLocaleValue(),
    locales,
  }).then(() => {
    ReactDOM.render(<OrderReviewCommercialComponentWithRouter />, document.getElementById(elementId));
  });
});
