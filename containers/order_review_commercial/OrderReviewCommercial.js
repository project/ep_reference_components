/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import ReactRouterPropTypes from 'react-router-prop-types';
import intl from 'react-intl-universal';
import Config from 'config';
import { login } from '../../common/AuthService';

import PaymentMethodContainer from '../../components/paymentmethod_container/paymentmethod.container';
import ShippingOptionContainer from '../../components/shippingoption_container/shippingoption.container';
import AddressContainer from '../../components/address_container/address.container';
import CheckoutSummaryCommercial from '../../components/checkout_summary_commercial/checkout.summary.commercial';
import { cortexFetch } from '../../common/Cortex';
import OrderTableMainCommercial from '../../components/ordertable_main_commercial/ordertable.main.commercial';

const zoomArray = [
  // zooms for checkout summary
  'defaultcart',
  'defaultcart:total',
  'defaultcart:discount',
  'defaultcart:order',
  'defaultcart:order:tax',
  'defaultcart:order:total',
  'defaultcart:appliedpromotions:element',
  'defaultcart:order:couponinfo:coupon',
  'defaultcart:order:couponinfo:couponform',
  // zoom for billing address
  'defaultcart:order:billingaddressinfo:billingaddress',
  // zoom for shipping address
  'defaultcart:order:deliveries:element:destinationinfo:destination',
  // zoom for shipping option
  'defaultcart:order:deliveries:element:shippingoptioninfo:shippingoption',
  // zoom for payment method
  'defaultcart:order:paymentmethodinfo:paymentmethod',
  // zooms for table items
  'defaultcart:lineitems:element',
  'defaultcart:lineitems:element:total',
  'defaultcart:lineitems:element:price',
  'defaultcart:lineitems:element:availability',
  'defaultcart:lineitems:element:item',
  'defaultcart:lineitems:element:item:code',
  'defaultcart:lineitems:element:item:price',
  'defaultcart:lineitems:element:item:definition',
  'defaultcart:lineitems:element:item:definition:options:element',
  'defaultcart:lineitems:element:item:definition:options:element:value',
  'defaultcart:lineitems:element:dependentlineitems',
  'defaultcart:lineitems:element:dependentlineitems:element',
  'defaultcart:lineitems:element:dependentlineitems:element:item:addtocartform',
  'defaultcart:lineitems:element:dependentlineitems:element:item:availability',
  'defaultcart:lineitems:element:dependentlineitems:element:item:definition',
  'defaultcart:lineitems:element:dependentlineitems:element:item:code',
  // zoom for purchaseform
  'defaultcart:order:purchaseform',
];

class OrderReviewCommercial extends React.Component {
  static propTypes = {
    history: ReactRouterPropTypes.history.isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {
      orderData: undefined,
    };
  }

  componentDidMount() {
    this.fetchOrderData();
  }

  fetchOrderData() {
    login().then(() => {
      cortexFetch(`/?zoom=${zoomArray.sort().join()}`,
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
          },
        })
        .then(res => res.json())
        .then((res) => {
          this.setState({
            orderData: res._defaultcart[0],
          });
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error.message);
        });
    });
  }

  completeOrder() {
    const purchaseZoomArray = [
      'paymentmeans:element',
      'shipments:element:destination',
      'shipments:element:shippingoption',
      'billingaddress',
      'discount',
      'appliedpromotions:element',
      'lineitems:element',
      'lineitems:element:options:element',
      'lineitems:element:options:element:value',
      'lineitems:element:item',
      'lineitems:element:item:availability',
      'lineitems:element:item:code',
      'lineitems:element:item:price',
      'lineitems:element:item:definition',
      'lineitems:element:item:definition:options:element',
      'lineitems:element:item:definition:options:element:value',
      'lineitems:element:components',
      'lineitems:element:components:element',
      'lineitems:element:components:element:item:addtocartform',
      'lineitems:element:components:element:item:availability',
      'lineitems:element:components:element:item:definition',
      'lineitems:element:components:element:item:code',
      'lineitems:element:dependentlineitems',
      'lineitems:element:dependentlineitems:element',
      'lineitems:element:dependentlineitems:element:item:addtocartform',
      'lineitems:element:dependentlineitems:element:item:availability',
      'lineitems:element:dependentlineitems:element:item:definition',
      'lineitems:element:dependentlineitems:element:item:code',
    ];
    const { orderData } = this.state;
    const { history } = this.props;
    const purchaseform = orderData._order[0]._purchaseform[0].links.find(link => link.rel === 'submitorderaction').uri;
    login().then(() => {
      cortexFetch(`${purchaseform}?followlocation&zoom=${purchaseZoomArray.sort().join()}`, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
        },
      })
        .then(res => res.json())
        .then((res) => {
          history.push('/purchaseReceipt', { data: res });
          window.location.reload();
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error.message);
        });
    });
  }

  /*
  trackTransactionAnalytics() {
    const { orderData } = this.state;
    if (isAnalyticsConfigured()) {
      const deliveries = (orderData._order[0]._deliveries) ? orderData._order[0]._deliveries[0]._element[0]._shippingoptioninfo[0]._shippingoption[0].cost[0].display : '';
      trackAddTransactionAnalytics(orderData.self.uri.split(`/carts/${Config.cortexApi.scope}/`)[1], orderData._order[0]._total[0].cost[0].amount, deliveries, orderData._order[0]._tax[0].total.display);
      orderData._lineitems[0]._element.map((product) => {
        const categoryTag = (product._item[0]._definition[0].details) ? (product._item[0]._definition[0].details.find(detail => detail['display-name'] === 'Tag')) : '';
        return (trackAddItemAnalytics(orderData.self.uri.split(`/carts/${Config.cortexApi.scope}/`)[1], product._item[0]._definition[0]['display-name'], product._item[0]._code[0].code, product._item[0]._price[0]['purchase-price'][0].display, (categoryTag !== undefined && categoryTag !== '') ? categoryTag['display-value'] : '', product.quantity));
      });
      sendAnalytics();
    }
  }
  */

  renderShippingOption() {
    const { orderData } = this.state;
    const deliveries = orderData._order[0]._deliveries;
    if (deliveries) {
      const [option] = orderData._order[0]._deliveries[0]._element[0]._shippingoptioninfo[0]._shippingoption;
      return (
        <div>
          <h3 className="shipping-billing-title">
            {intl.get('shipping-option')}
          </h3>
          <ShippingOptionContainer option={option} />
        </div>
      );
    }
    return null;
  }

  renderShippingAddress() {
    const { orderData } = this.state;
    const deliveries = orderData._order[0]._deliveries;
    if (deliveries) {
      const [shippingAddress] = orderData._order[0]._deliveries[0]._element[0]._destinationinfo[0]._destination;
      const { name, address } = shippingAddress;
      return (
        <div>
          <h3 className="shipping-billing-title">
            {intl.get('shipping-address')}
          </h3>
          <AddressContainer name={name} address={address} />
        </div>
      );
    }
    return null;
  }

  renderBillingAddress() {
    const { orderData } = this.state;
    const [billingAddress] = orderData._order[0]._billingaddressinfo[0]._billingaddress;
    const { name, address } = billingAddress;
    return (
      <div>
        <h3 className="shipping-billing-title">
          {intl.get('billing-address')}
        </h3>
        <AddressContainer name={name} address={address} />
      </div>
    );
  }

  renderPaymentMethod() {
    const { orderData } = this.state;
    const displayName = orderData._order[0]._paymentmethodinfo[0]._paymentmethod[0]['display-name'];
    return (
      <div>
        <h3 className="shipping-billing-title">
          {intl.get('payment-method')}
        </h3>
        <PaymentMethodContainer displayName={displayName} />
      </div>
    );
  }

  render() {
    const { orderData } = this.state;
    return (
      <div>
        <div className="order-commercial container">
          <div className="order-commercial-inner">
            <div className="order-title-container">
              <div>
                <h1 className="view-title">
                  {intl.get('review-your-order')}
                </h1>
              </div>
              <div className="order-title-row row">
                <div className="col-md order-col-prd text-uppercase">
                  {intl.get('product')}
                </div>
                <div className="col-md" />
                <div className="col-md order-col-availability text-uppercase">
                  {intl.get('availability')}
                </div>
                <div className="col-md order-col-qty text-uppercase">
                  {intl.get('quantity')}
                </div>
                <div className="col-md order-col-each text-uppercase">
                  {intl.get('price')}
                </div>
                <div className="col-md order-col-total text-uppercase">
                  {intl.get('total')}
                </div>
              </div>
            </div>
            {orderData && (
              <div className="order-main-container">
                <div className="order-items-container">
                  <OrderTableMainCommercial data={orderData} />
                </div>
                <div className="container order-section">
                  <div className="order-label-row">
                    {intl.get('order-shipping')}
                  </div>
                  <div className="shipping-info row">
                    <div className="col-6 col-md-4">
                      {this.renderShippingAddress()}
                    </div>
                    <div className="col-6 col-md-8">
                      {this.renderShippingOption()}
                    </div>
                  </div>
                </div>
                <div className="container order-section">
                  <div className="order-label-row">
                    {intl.get('order-billing')}
                  </div>
                  <div className="billing-info row">
                    <div className="col-6 col-md-4">
                      {this.renderBillingAddress()}
                    </div>
                    <div className="col-6 col-md-8">
                      {this.renderPaymentMethod()}
                    </div>
                  </div>
                </div>
              </div>
            )}
            {orderData && (
              <div className="checkout-sidebar">
                <div className="checkout-sidebar-inner">
                  <div className="d-flex flex-row justify-content-end">
                    <div className="checkout-summary-container">
                      <CheckoutSummaryCommercial data={orderData} isLoading={false} />
                    </div>
                  </div>
                  <div className="d-sm-flex flex-row justify-content-between">
                    <button className="ep-btn secondary wide order-review-btn" type="button" onClick={() => { window.location.href = '/checkout'; }}>
                      {intl.get('back-checkout')}
                    </button>
                    <button className="ep-btn primary wide order-review-btn" type="button" onClick={() => { this.completeOrder(); }}>
                      {intl.get('complete-purchase')}
                    </button>
                  </div>
                </div>
              </div>
            )}
            {!orderData && (
              <div className="order-main-container">
                <div className="loader" />
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default OrderReviewCommercial;
