/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import intl from 'react-intl-universal';
import Config from 'config';
import { login } from '../../common/AuthService';
import { itemLookup, cortexFetchItemLookupForm } from '../../common/CortexLookup';
import { cortexFetch } from '../../common/Cortex';
import BatchQuickOrderModalMain from '../../components/batch_quick_order_entry_modal/batchquickordermodal.main';

// import Modal from 'react-responsive-modal';


class BatchQuickOrderEntry extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openModal: false,
    };
    this.handleModalClose = this.handleModalClose.bind(this);
  }

  quickOrder() {
    const sku = document.getElementById('quick-order-sku').value;
    const qty = document.getElementById('quick-order-qty').value;
    if (sku && qty) {
      login().then(() => {
        cortexFetchItemLookupForm().then(() => {
          itemLookup(sku).then((productData) => {
            if (productData._addtocartform[0].links.length > 0) {
              this.addToCart(productData, qty);
            } else {
              document.getElementById('quick-order-error').innerHTML = intl.get('item-not-available');
            }
          })
            .catch(() => {
              document.getElementById('quick-order-error').innerHTML = intl.get('no-sku-item').replace('%sku%', sku);
            });
        });
      });
    } else {
      document.getElementById('quick-order-error').innerHTML = intl.get('missing-fields');
    }
  }

  handleModalOpen() {
    this.setState({
      openModal: true,
    });
  }

  handleModalClose() {
    this.setState({
      openModal: false,
    });
  }

  batchAddToCart(list) {
    const body = {
      items: list,
    };
    const { cartData } = this.state;
    login().then(() => {
      const addToCartLink = cartData['_batch-add-to-cart-form'][0].links.find(link => link.rel === 'submit-action');
      cortexFetch(addToCartLink.uri,
        {
          method: 'post',
          headers: {
            'Content-Type': 'application/json',
            Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
          },
          body: JSON.stringify(body),
        })
        .then((res) => {
          if (res.status === 200 || res.status === 201) {
            window.location = '/mybag';
          } else {
            let debugMessages = '';
            res.json().then((json) => {
              for (let i = 0; i < json.messages.length; i++) {
                debugMessages = debugMessages.concat(`- ${json.messages[i]['debug-message']} \n `);
              }
              document.getElementById('batch-add-to-cart-error').innerHTML = debugMessages;
            });
          }
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error.message);
        });
    }).catch((error) => {
      // eslint-disable-next-line no-console
      console.error(error.message);
    });
  }

  render() {
    const {
      openModal,
    } = this.state;
    return (
      <div className="batch-quick-order-container container">
        <h1 className="subtitle-label">{intl.get('quick-order-entry')}</h1>
        <button
          className="ep-btn primary wide btn-itemdetail-addtocart"
          id="quick_order_item_add_to_cart_button"
          type="button"
          data-toggle="modal"
          onClick={() => this.handleModalOpen()}
          data-target="#quick-order-modal"
        >
          {intl.get('input-items')}
        </button>
        <BatchQuickOrderModalMain key="app-modal-login-main" handleModalClose={this.handleModalClose} openModal={openModal} />
      </div>
    );
  }
}

export default BatchQuickOrderEntry;
