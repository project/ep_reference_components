/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import intl from 'react-intl-universal';
import AppHeaderMain from './appheadernavigation.main';
import onComponentLoad from '../../common/onComponentLoad';
import withDataAttributes from '../../components/util/dataattribute';
import locales from '../../common/locales';
import { getSelectedLocaleValue } from '../../common/UserPrefs';

const NavigationWrapper = (props) => {
  const { dataSet } = props;

  return (
    <>
      <div className="main-container">
        <AppHeaderMain dataSet={dataSet} isMobileView={false} />
      </div>
      <div className="collapsable-container collapse collapsed">
        <AppHeaderMain dataSet={dataSet} isMobileView />
      </div>
    </>
  );
};

NavigationWrapper.propTypes = {
  dataSet: PropTypes.objectOf(PropTypes.object).isRequired,
};

const AppHeaderMainWithAttributes = withDataAttributes(NavigationWrapper);

onComponentLoad('ep-appheadermain', (elementId) => {
  intl.init({
    currentLocale: getSelectedLocaleValue(),
    locales,
  }).then(() => {
    ReactDOM.render(
      <AppHeaderMainWithAttributes />,
      document.getElementById(elementId),
    );
  });
});
