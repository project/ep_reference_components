/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import { Parallax } from 'react-parallax';
import PropTypes from 'prop-types';
import FeaturedItemList from '../../components/featured_items/featureditemlist.main';
import homeEspotParallax1 from '../../style/images/site-images/car-inside.jpg';

const FeaturedParallax = (props) => {
  const { dataSet } = props;
  const imageURL = dataSet.parallaxImageUrl;

  return (
    <div className="featured-parallax-component" data-region="viewPortRegion">
      <div className="section-parallax section-parallax-1 container" data-region="homeMainContentRegion">
        <Parallax className="parallax" bgImage={imageURL} bgImageAlt="home-espot-1" bgClassName="parallax-image" strength={600} onError={(e) => { e.target.bgImage = homeEspotParallax1; }}>
          <div style={{ height: '100vh' }} />
        </Parallax>
        <div className="sub-section container-fluid">
          <FeaturedItemList propertiesMap={dataSet} />
        </div>
      </div>
    </div>
  );
};

FeaturedParallax.propTypes = {
  dataSet: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default FeaturedParallax;
