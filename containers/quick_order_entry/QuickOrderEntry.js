/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import intl from 'react-intl-universal';
import Config from 'config';
import { login } from '../../common/AuthService';
import { itemLookup, cortexFetchItemLookupForm } from '../../common/CortexLookup';
import { cortexFetch } from '../../common/Cortex';


class QuickOrderEntry extends React.Component {
  quickOrder() {
    const sku = document.getElementById('quick-order-sku').value;
    const qty = document.getElementById('quick-order-qty').value;
    if (sku && qty) {
      login().then(() => {
        cortexFetchItemLookupForm().then(() => {
          itemLookup(sku).then((productData) => {
            if (productData._addtocartform[0].links.length > 0) {
              this.addToCart(productData, qty);
            } else {
              document.getElementById('quick-order-error').innerHTML = intl.get('item-not-available');
            }
          })
            .catch(() => {
              document.getElementById('quick-order-error').innerHTML = intl.get('no-sku-item').replace('%sku%', sku);
            });
        });
      });
    } else {
      document.getElementById('quick-order-error').innerHTML = intl.get('missing-fields');
    }
  }

  // eslint-disable-next-line class-methods-use-this
  addToCart(productData, qty) {
    const addToCartLink = productData._addtocartform[0].links.find(link => link.rel === 'addtodefaultcartaction');
    const body = {};
    body.quantity = qty;
    cortexFetch(addToCartLink.uri,
      {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
        },
        body: JSON.stringify(body),
      })
      .then((res) => {
        if (res.status === 200 || res.status === 201) {
          window.location.reload();
        } else {
          let debugMessages = '';
          res.json().then((json) => {
            for (let i = 0; i < json.messages.length; i++) {
              debugMessages = debugMessages.concat(`- ${json.messages[i]['debug-message']} \n `);
            }
          });
        }
      });
  }

  render() {
    return (
      <div className="quick-order-container container form-group">
        <h1 className="subtitle-label">
          {intl.get('quick-order-entry')}
        </h1>
        <p className="error-label" id="quick-order-error" />
        <div>
          <input type="text" className="form-control quick-order-form" id="quick-order-sku" placeholder={intl.get('enter-sku')} />
          <input type="number" className="form-control quick-order-form" min="1" id="quick-order-qty" placeholder={intl.get('enter-quantity')} />
        </div>
        <button
          className="ep-btn primary wide btn-itemdetail-addtocart"
          id="quick_order_item_add_to_cart_button"
          type="submit"
          onClick={this.quickOrder.bind(this)}
        >
          {intl.get('add-to-cart-b2b')}
        </button>
      </div>
    );
  }
}

export default QuickOrderEntry;
