# EP Reference Components Module
This module provides a collection of EP-integrated progressively decoupled React
blocks, and makes them available within Drupal. They combine the strengths of
using React as a front-end framework, with the layout customization provided
by Drupal.

This project is primarily a port of the [React PWA Reference Storefront
](https://github.com/elasticpath/react-pwa-reference-storefront)

## Architecture Overview - TBD

## Component Styling
All EP-integrated progressively decoupled React blocks will contain their own OOTB styling.  If styling changes are desired there are two approaches
1.  Change the styling directly under `ep_reference_components/style/less`.
2.  Create a new Drupal theme and override existing styles.

##### How do I override my decoupled react blocks using a Drupal Theme?

1. Create a new theme
2. Copy all contents of `ep_reference_components/style/` into the root of your theme
3. Ensure that all less files that have been moved over into theme have been compiled into CSS.
4. Open to `<THEMENAME>.info.yml` file
5. In `<THEMENAME>.info.yml` paste:

```libraries-override:
  # Override the containers
  pdb/<machine_name>/footer:
    css:
      component:
        <container_path>/../../style/<style_path>: <style_path>
```

at the bottom of yml file.

6. Replace `machine_name` with the machine name of the `ep_reference_component` block you'd like to override.  

7. Replace `container_path` with the path to the block container inside the `ep_reference_components` Ex. `/modules/custom/ep_reference_components/containers/app_header_main`

8. Replace `style_path` with the path of the css file you'd like to override in `ep_reference_components` Ex. `css/containers/app_header_main/appheadernavigation.main.css`

Note: You can find an example of overridden styles in the `elasticpath_commercial` theme.

9. If you'd like to override another style present in the same block, copy `<container_path>/../../style/<style_path>: <style_path>`, change variables accordingly, and place in the next line.

## Linting
The EP Reference Components module uses the same ESLint rules as the [React PWA
Reference Storefront](https://github.com/elasticpath/react-pwa-reference-storefront).
Make sure you run `npm install` in the `ep_reference_components` directory so
that the local eslint binary is installed.