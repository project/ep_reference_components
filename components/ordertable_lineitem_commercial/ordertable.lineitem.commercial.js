/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import intl from 'react-intl-universal';


const OrderTableLineItemCommercial = (props) => {
  const { item } = props;
  const displayName = item._item[0]._definition[0]['display-name'];
  const options = item._item[0]._definition[0]._options;
  const { quantity } = item;
  const itemPrice = ((item._price) ? (item._price) : (item._item[0]._price));
  const listPrice = itemPrice[0]['list-price'][0].display;
  const purchasePrice = itemPrice[0]['purchase-price'][0].display;
  const totalPrice = (item._total) ? item._total[0].cost[0].display : item['line-extension-total'][0].display;
  const itemAvailability = ((item._availability) ? (item._availability) : (item._item[0]._availability));
  let availability = (itemAvailability[0].state === 'AVAILABLE');
  let availabilityString = '';
  if (itemAvailability.length >= 0) {
    if (itemAvailability[0].state === 'AVAILABLE') {
      availability = true;
      availabilityString = intl.get('in-stock');
    } else if (itemAvailability[0].state === 'AVAILABLE_FOR_PRE_ORDER') {
      availability = true;
      availabilityString = intl.get('pre-order');
    } else if (itemAvailability[0].state === 'AVAILABLE_FOR_BACK_ORDER') {
      availability = true;
      availabilityString = intl.get('back-order');
    } else {
      availability = false;
      availabilityString = intl.get('out-of-stock');
    }
  }

  const renderBundleConfiguration = () => {
    let bundleConfigs = null;
    if (item._components) {
      bundleConfigs = (item._components && item._components[0] && item._components[0]._element) ? (item._components[0]._element) : (null);
    }
    if (item._dependentlineitems) {
      bundleConfigs = (item._dependentlineitems && item._dependentlineitems[0] && item._dependentlineitems[0]._element) ? (item._dependentlineitems[0]._element) : (null);
    }
    if (bundleConfigs) {
      return bundleConfigs.map(config => (
        <li className="bundle-configuration" key={config}>
          <label htmlFor="option-name" className="option-name">
            {config._item[0]._definition[0]['display-name']}
            &nbsp;
          </label>
        </li>
      ));
    }
    return null;
  };

  const renderConfiguration = () => {
    const keys = (item.configuration) ? (Object.keys(item.configuration)) : ('');
    if (keys) {
      return keys.map(key => (
        <li className="configuration" key={key}>
          <label htmlFor="option-name" className="option-name">
            {key}
            :&nbsp;
          </label>
          <span>
            {item.configuration[key]}
          </span>
        </li>
      ));
    }
    return null;
  };

  const renderOptions = () => {
    if (options) {
      return (
        options[0]._element.map(option => (
          <li className="order-lineitem-option" key={option['display-name']}>
            <label htmlFor="order-lineitem-option-value" className="order-lineitem-option-name">
              {option['display-name']}
              :&nbsp;
            </label>
            <span className="order-lineitem-option-value">
              {option._value[0]['display-name']}
            </span>
          </li>
        ))
      );
    }
    return null;
  };

  return (
    <div className="order-lineitem-row">
      <div className="row order-row">
        <div className="order-col-prd col-md">
          <div className="title-col">
            {displayName}
          </div>
        </div>
        <div className="options-col col-md">
          <ul className="options-container">
            {renderOptions()}
            {renderConfiguration()}
            {renderBundleConfiguration()}
          </ul>
        </div>
        <div className="order-col-availability col-md">
          <div className="availability-col">
            <ul className="availability-container">
              <li className="availability itemdetail-availability-state" data-i18n="AVAILABLE">
                <div>
                  {availability && <span className="icon" />}
                  {availabilityString}
                </div>
              </li>
              <li className={`category-item-release-date${itemAvailability[0]['release-date'] ? '' : ' is-hidden'}`} data-region="itemAvailabilityDescriptionRegion">
                <label htmlFor="release-date-value" className="releasedate-label">
                  {intl.get('expected-release-date')}
                  :&nbsp;
                </label>
                <span className="release-date-value">
                  {(itemAvailability[0]['release-date']) ? itemAvailability[0]['release-date']['display-value'] : ''}
                </span>
              </li>
            </ul>
          </div>
        </div>
        <div className="order-col-qty col-md">
          <div className="mobile-title text-uppercase">
            {intl.get('quantity')}
          </div>
          <div className="quantity-val">
            {quantity}
          </div>
        </div>
        <div className="order-col-each col-md">
          <div className="unit-price-col" data-region="cartLineitemUnitPriceRegion">
            <div>
              <div data-region="itemUnitPriceRegion">
                <span className="mobile-title text-uppercase">
                  {intl.get('unit-price')}
                </span>
                {listPrice === purchasePrice ? (
                  <ul className="price-container">
                    <li className="order-unit-purchase-price">
                      {purchasePrice}
                    </li>
                  </ul>
                ) : (
                  <ul className="price-container">
                    <li className="order-unit-list-price" data-region="itemListPriceRegion">
                      {listPrice}
                    </li>
                    <li className="order-unit-purchase-price">
                      {purchasePrice}
                    </li>
                  </ul>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="order-col-total col-md">
          <div>
            <div className="mobile-title text-uppercase">
              {intl.get('total-price')}
            </div>
            <div className="purchase-price">
              {totalPrice}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

OrderTableLineItemCommercial.propTypes = {
  item: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default OrderTableLineItemCommercial;
