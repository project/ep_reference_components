/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import intl from 'react-intl-universal';
import PaymentMethodContainer from '../paymentmethod_container/paymentmethod.container';
import ShippingOptionContainer from '../shippingoption_container/shippingoption.container';
import AddressContainer from '../address_container/address.container';

const PurchaseDetailsMain = (props) => {
  const { data } = props;
  const { status } = data;
  let statusString;
  switch (status) {
    case 'CANCELED':
      statusString = intl.get('cancelled');
      break;
    case 'COMPLETED':
      statusString = intl.get('completed');
      break;
    default:
      statusString = intl.get('in-progress');
  }
  const orderNumber = data['purchase-number'];
  const orderTaxTotal = data['tax-total'].display;
  const orderPurchaseDate = new Date(data['purchase-date'].value);
  const orderDiscount = data._discount[0].discount[0].display;
  const orderTotal = data['monetary-total'][0].display;

  const shipments = data._shipments;

  const renderShippingOption = () => {
    if (shipments) {
      const [option] = data._shipments[0]._element[0]._shippingoption;
      return (
        <div style={{ display: 'inline-block', paddingLeft: '20px' }}>
          <h3 className="shipping-billing-title">
            {intl.get('shipping-option')}
          </h3>
          <ShippingOptionContainer option={option} />
        </div>
      );
    }
    return null;
  };

  const renderShippingAddress = () => {
    if (shipments) {
      const [shippingAddress] = data._shipments[0]._element[0]._destination;
      const { name, address } = shippingAddress;
      return (
        <div style={{ display: 'inline-block', paddingLeft: '20px' }}>
          <h3 className="shipping-billing-title">
            {intl.get('shipping-address')}
          </h3>
          <AddressContainer name={name} address={address} />
        </div>
      );
    }
    return null;
  };

  const renderBillingAddress = () => {
    const [billingAddress] = data._billingaddress;
    const { name, address } = billingAddress;
    return (
      <div style={{ display: 'inline-block', paddingLeft: '20px' }}>
        <h3 className="shipping-billing-title">
          {intl.get('billing-address')}
        </h3>
        <AddressContainer name={name} address={address} />
      </div>
    );
  };

  const renderPaymentMethod = () => {
    const displayName = data._paymentmeans[0]._element[0]['display-name'];
    return (
      <div style={{ display: 'inline-block', paddingLeft: '20px', verticalAlign: 'top' }}>
        <h3 className="shipping-billing-title">
          {intl.get('payment-method')}
        </h3>
        <PaymentMethodContainer displayName={displayName} />
      </div>
    );
  };

  const calculateTotal = (tax, itemPrice) => {
    const total = itemPrice + tax;
    return `$ ${total.toFixed(2)}`;
  };

  const calcPricePerUnit = (subtotal, qty) => {
    const total = subtotal / qty;
    return `$ ${total.toFixed(2)}`;
  };

  const renderPurchaseDate = () => (
    <div className="purchase-summary-date purchase-summary-row">
      <div>
        {intl.get('order-purchase-date')}
      </div>
      <div>
        {orderPurchaseDate.toLocaleDateString('en-US')}
      </div>
    </div>
  );

  const renderOrderNumber = () => (
    <div className="purchase-summary-order-number purchase-summary-row">
      <div>
        {intl.get('order-number')}
      </div>
      <div>
        {orderNumber}
      </div>
    </div>
  );

  const renderOrderStatus = () => (
    <div className="purchase-summary-order-status purchase-summary-row">
      <div>
        {intl.get('status')}
      </div>
      <div>
        {statusString}
      </div>
    </div>
  );

  const renderTaxTotal = () => (
    <div className="purchase-summary-tax-total purchase-summary-row">
      <div>
        {intl.get('order-tax-total')}
      </div>
      <div>
        {orderTaxTotal}
      </div>
    </div>
  );

  const renderDiscount = () => (
    <div className="purchase-summary-discount purchase-summary-row">
      <div>
        {intl.get('todays-discount')}
      </div>
      <div>
        {orderDiscount}
      </div>
    </div>
  );

  const renderOrderTotal = () => (
    <div className="purchase-summary-total purchase-summary-row">
      <div>
        {intl.get('order-total')}
      </div>
      <div>
        {orderTotal}
      </div>
    </div>
  );

  const renderItem = (purchaseItem) => {
    // console.log(purchaseItem);
    const { name, quantity } = purchaseItem;
    const subTotal = purchaseItem['line-extension-amount'][0].amount;
    const tax = purchaseItem['line-extension-tax'][0].display;
    // const itemTotal = purchaseItem['line-extension-total'][0].display;
    const options = purchaseItem._options;
    const configuration = purchaseItem._configuration;
    const bundleConfiguration = purchaseItem._components;
    return (
      <div className="purchase-lineitem-row">
        <div className="row purchase-row">
          <div className="purchase-col-prd col-md">
            <div className="title-col">
              {name}
            </div>
          </div>
          <div className="options-col col-md">
            <ul className="options-container">
              <td className="options-col" style={{ display: 'table-cell' }}>
                <ul className="options-container">
                  {options && (options[0]._element.map(option => (
                    <li className="purchase-lineitem-option" key={option['display-name']}>
                      <label htmlFor="purchase-lineitem-option-value" className="purchase-lineitem-option-name">
                        {option['display-name']}
                  :&nbsp;
                      </label>
                      <span className="purchase-lineitem-option-value">
                        {option._value[0]['display-name']}
                      </span>
                    </li>
                  )))}
                  {configuration && (configuration[0]._element.map(config => (
                    <li className="configuration" key={config.name}>
                      <label htmlFor="option-name" className="option-name">
                        {config['display-name']}
                  :&nbsp;
                      </label>
                      <span>
                        {config._value[0]['display-name']}
                      </span>
                    </li>
                  )))}
                  {bundleConfiguration && (bundleConfiguration[0]._element.map(config => (
                    <li className="bundle-configuration" key={config.name}>
                      <label htmlFor="option-name" className="option-name">
                        {config.name}
                  &nbsp;
                      </label>
                    </li>
                  )))}
                </ul>
              </td>
            </ul>
          </div>
          <div className="purchase-col-qty col-md">
            <div className="mobile-title text-uppercase">
              {intl.get('quantity')}
            </div>
            <div className="quantity-val">
              {quantity}
            </div>
          </div>
          <div className="purchase-col-each col-md">
            <div className="unit-price-col" data-region="cartLineitemUnitPriceRegion">
              <div>
                <div data-region="itemUnitPriceRegion">
                  <span className="mobile-title text-uppercase">
                    {intl.get('unit-price')}
                  </span>
                  <ul className="price-container">
                    <li className="purchase-unit-purchase-price">
                      {calcPricePerUnit(subTotal, quantity)}
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="purchase-col-tax col-md">
            <div>
              <div className="mobile-title text-uppercase">
                {intl.get('tax')}
              </div>
              <div className="tax-price">
                {tax}
              </div>
            </div>
          </div>
          <div className="purchase-col-total col-md">
            <div>
              <div className="mobile-title text-uppercase">
                {intl.get('total-price')}
              </div>
              <div className="purchase-price">
                {/* {itemTotal} */}
                {calculateTotal(purchaseItem['line-extension-tax'][0].amount, purchaseItem['line-extension-amount'][0].amount)}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div data-region="purchaseInformationRegion" style={{ display: 'block' }}>
      <div className="purchase-information-container container">
        <div data-region="purchaseSummaryRegion" style={{ display: 'block' }}>
          <div>
            <div data-region="purchaseLineItemsRegion" className="purchase-items-container" style={{ display: 'block' }}>
              <div className="purchase-main-inner">
                {data._lineitems[0]._element.map(renderItem)}
              </div>
            </div>
          </div>
        </div>
        <div className="purchase-options-container" style={{ display: 'block', paddingBottom: 'px' }}>
          <div className="container purchase-section">
            <div className="purchase-label-row">
              {intl.get('order-shipping')}
            </div>
            <div className="shipping-info row">
              <div className="col-6 col-md-4">
                {renderShippingAddress()}
              </div>
              <div className="col-6 col-md-8">
                {renderShippingOption()}
              </div>
            </div>
          </div>
          <div className="container purchase-section">
            <div className="purchase-label-row">
              {intl.get('order-billing')}
            </div>
            <div className="billing-info row">
              <div className="col-6 col-md-4">
                {renderBillingAddress()}
              </div>
              <div className="col-6 col-md-8">
                {renderPaymentMethod()}
              </div>
            </div>
          </div>
          <div className="purchase-sidebar">
            <div className="purchase-sidebar-inner">
              <div className="d-flex flex-row justify-content-end">
                <div className="purchase-summary-container">
                  <div className="purchase-summary-commercial">
                    {renderPurchaseDate()}
                    {renderOrderNumber()}
                    {renderOrderStatus()}
                    {renderTaxTotal()}
                    {renderDiscount()}
                    {renderOrderTotal()}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

PurchaseDetailsMain.propTypes = {
  data: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default PurchaseDetailsMain;
