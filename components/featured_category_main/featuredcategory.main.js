/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import Config from 'config';
import imgPlaceholder from '../../style/images/image-placeholder_Vestri.png';


class FeaturedCategoryMain extends React.Component {
  static isLoggedIn() {
    return (localStorage.getItem(`${Config.cortexApi.scope}_oAuthRole`) === 'REGISTERED');
  }

  static propTypes = {
    propertiesMap: PropTypes.objectOf(PropTypes.object).isRequired,
  }

  render() {
    const { propertiesMap } = this.props;
    const {
      orientation,
      categoryCode,
      categoryDescription,
      buttonText,
      imageUrl,
    } = propertiesMap;

    return (
      <div className="featured-category-component">
        {
          orientation === 'LEFT'
          && (
          <div className="image-container">
            <img className="category-image left-image" alt="home-espot-3" src={imageUrl} onError={(e) => { e.target.src = imgPlaceholder; }} />
          </div>
          )
        }
        <div className="text-container">
          <div className="content-box">
            <div className="text-box">
              <p className="category-description">{categoryDescription}</p>
              <div className="gradient-box" />
            </div>
            <div>
              <button
                className="ep-btn primary wide"
                type="submit"
                // eslint-disable-next-line func-names
                onClick={function () { window.location.href = `/category/${categoryCode}`; }}
              >
                {buttonText}
              </button>
            </div>
          </div>
        </div>
        {
          orientation === 'RIGHT'
          && (
          <div className="image-container">
            <img className="category-image right-image" alt="home-espot-3" src={imageUrl} onError={(e) => { e.target.src = imgPlaceholder; }} />
          </div>
          )
        }
      </div>
    );
  }
}

export default FeaturedCategoryMain;
