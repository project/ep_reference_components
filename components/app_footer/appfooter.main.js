/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import intl from 'react-intl-universal';

function AppFooterMain() {
  return (
    <footer className="app-footer">
      <div className="first-row">
        <div className="footer-column">
          <div className="title">
            {intl.get('vestri')}
          </div>
          <div className="content">
            {intl.get('store-description-message')}
          </div>
        </div>
        <div className="footer-column">
          <div className="title">
            {intl.get('assistance')}
          </div>
          <div className="content">
            <a href="/aboutus">
              {intl.get('about-us')}
            </a>
            <a href="/contactus">
              {intl.get('contact')}
            </a>
            <a href="/shippingreturns">
              {intl.get('shipping-returns')}
            </a>
            <a href="/termsandconditions">
              {intl.get('terms-and-conditions')}
            </a>
          </div>
        </div>
        <div className="footer-column social">
          <div className="title">
            {intl.get('find-us-online')}
          </div>
          <div className="content">
            <a href="/" aria-label="share facebook">
              <span className="share-icon facebook" />
              {intl.get('facebook')}
            </a>
            <a href="/" aria-label="share twitter">
              <span className="share-icon twitter" />
              {intl.get('twitter')}
            </a>
            <a href="/" alt="share instagram">
              <span className="share-icon instagram" />
              {intl.get('instagram')}
            </a>
          </div>
        </div>
      </div>
      <div className="second-row">
        <div className="title">
          {intl.get('find-us-online')}
        </div>
        <a href="/" aria-label="share facebook">
          <span className="share-icon facebook" />
        </a>
        <a href="/" aria-label="share twitter">
          <span className="share-icon twitter" />
        </a>
        <a href="/" aria-label="share instagram">
          <span className="share-icon instagram" />
        </a>
      </div>
    </footer>
  );
}

export default AppFooterMain;
