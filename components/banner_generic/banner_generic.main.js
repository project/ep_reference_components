/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Config from 'config';
import intl from 'react-intl-universal';
import AppModalLoginMain from '../../containers/app_modal_login_container/appmodalogin.main';

class BannerGenericMain extends React.Component {
  static propTypes = {
    leadingText: PropTypes.string,
    tagline: PropTypes.string,
    markingLine: PropTypes.string,
    backgroundImageUrl: PropTypes.string,
    bannerColour: PropTypes.string,
    textColour: PropTypes.string,
  };

  static defaultProps = {
    leadingText: '',
    tagline: '',
    markingLine: '',
    backgroundImageUrl: '',
    bannerColour: '',
    textColour: '',
  };

  static isLoggedIn() {
    const OAuthRole = localStorage
      .getItem(`${Config.cortexApi.scope}_oAuthRole`);

    return (OAuthRole === 'REGISTERED');
  }

  constructor(props) {
    super(props);
    this.state = {
      openModal: false,
    };
    this.handleModalClose = this.handleModalClose.bind(this);
  }

  componentDidMount() {
    if (Config.b2b.enable && localStorage.getItem(`${Config.cortexApi.scope}_oAuthTokenAuthService`) !== null && localStorage.getItem(`${Config.cortexApi.scope}_b2bCart`) === null) {
      this.handleCartModalOpen();
    }
  }

  handleModalOpen() {
    this.setState({
      openModal: true,
    });
  }

  handleModalClose() {
    this.setState({
      openModal: false,
    });
  }

  render() {
    const {
      leadingText,
      tagline,
      markingLine,
      backgroundImageUrl,
      bannerColour,
      textColour,
    } = this.props;

    const {
      openModal,
    } = this.state;

    const configurableBackgroundStyle = {
      backgroundImage: `url(${backgroundImageUrl})`,
    };

    const configurableBannerStyle = {
      backgroundColor: bannerColour,
      color: textColour,
    };

    const keycloakLoginRedirectUrl = Config.b2b.enable
      ? `${Config.b2b.keycloak.loginRedirectUrl}?client_id=${Config.b2b.keycloak.client_id}&response_type=code&scope=openid&redirect_uri=${encodeURIComponent(Config.b2b.keycloak.callbackUrl)}`
      : '';

    return (
      <div
        className="bannergeneric"
        style={configurableBackgroundStyle}
      >
        <div className="content-column-wrapper apply-side-whitespace">
          <div className="bannergeneric-content-wrapper" style={configurableBannerStyle}>
            {leadingText && <h2 className="bannergeneric-leadingtext">{leadingText}</h2> }
            {tagline && <h1 className="bannergeneric-tagline">{tagline}</h1> }
            {markingLine && <h2 className="bannergeneric-markingline">{markingLine}</h2> }
            {!BannerGenericMain.isLoggedIn() && (
              (Config.b2b.enable) ? (
                <a href={`${keycloakLoginRedirectUrl}`} className="login-auth-service-btn">
                  <button type="button" className="btn ep-btn primary bannergeneric-login-button">
                    { intl.get('login') }
                  </button>
                </a>
              ) : (
                <div>
                  <button
                    type="button"
                    className="btn ep-btn primary bannergeneric-login-button"
                    data-toggle="modal"
                    onClick={() => this.handleModalOpen()}
                    data-target="#login-modal"
                  >
                    { intl.get('click-to-login') }
                  </button>
                  <AppModalLoginMain
                    key="app-modal-login-main"
                    handleModalClose={this.handleModalClose}
                    openModal={openModal}
                  />
                </div>
              )
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default BannerGenericMain;
