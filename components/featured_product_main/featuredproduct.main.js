/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import intl from 'react-intl-universal';
import Config from 'config';
import { login } from '../../common/AuthService';
import { itemLookup, cortexFetchItemLookupForm } from '../../common/CortexLookup';
import imgPlaceholder from '../../style/images/image-placeholder_Vestri.png';
import { cortexFetch } from '../../common/Cortex';

class FeaturedProductMain extends React.Component {
  static isLoggedIn() {
    return (localStorage.getItem(`${Config.cortexApi.scope}_oAuthRole`) === 'REGISTERED');
  }

  static propTypes = {
    propertiesMap: PropTypes.objectOf(PropTypes.object).isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {
      productData: {},
    };
  }

  componentDidMount() {
    const { propertiesMap } = this.props;
    const skuList = [propertiesMap.sku1, propertiesMap.sku2, propertiesMap.sku3];
    this.fetchProducts(skuList[Math.floor(Math.random() * 3)]);
  }

  addToCart(event) {
    const { productData } = this.state;
    login().then(() => {
      const addToCartLink = productData._addtocartform[0].links.find(link => link.rel === 'addtodefaultcartaction');
      const body = {};
      body.quantity = 1;
      cortexFetch(addToCartLink.uri,
        {
          method: 'post',
          headers: {
            'Content-Type': 'application/json',
            Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
          },
          body: JSON.stringify(body),
        })
        .then((res) => {
          if (res.status === 200 || res.status === 201) {
            window.location.href = '/mybag';
          } else {
            let debugMessages = '';
            res.json().then((json) => {
              for (let i = 0; i < json.messages.length; i++) {
                debugMessages = debugMessages.concat(`- ${json.messages[i]['debug-message']} \n `);
              }
            });
          }
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error.message);
        });
    });
    event.preventDefault();
  }

  addToWishList(event) {
    const { productData } = this.state;
    login().then(() => {
      const addToWishListLink = productData._addtowishlistform[0].links.find(link => link.rel === 'addtodefaultwishlistaction');
      const body = {};
      body.quantity = 1;
      cortexFetch(addToWishListLink.uri,
        {
          method: 'post',
          headers: {
            'Content-Type': 'application/json',
            Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
          },
          body: JSON.stringify(body),
        })
        .then((res) => {
          if (res.status === 200 || res.status === 201) {
            window.location.href = '/wishlists';
            // history.push('/wishlists');
          } else {
            let debugMessages = '';
            res.json().then((json) => {
              for (let i = 0; i < json.messages.length; i++) {
                debugMessages = debugMessages.concat(`- ${json.messages[i]['debug-message']} \n `);
              }
            });
          }
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error.message);
        });
    });
    event.preventDefault();
  }

  fetchProducts(sku) {
    login().then(() => {
      cortexFetchItemLookupForm().then(() => {
        itemLookup(sku).then((productData) => {
          this.setState({ productData });
        })
          .catch((error) => {
            // eslint-disable-next-line no-console
            console.error(error.message);
          });
      });
    });
  }

  render() {
    const { productData } = this.state;
    const { propertiesMap } = this.props;
    const { orientation } = propertiesMap;

    if (!(Object.keys(productData).length === 0)) {
      const listPrice = (productData._price) ? productData._price[0]['list-price'][0].display : 'n/a';
      const itemPrice = (productData._price) ? productData._price[0]['purchase-price'][0].display : 'n/a';
      const productDescription = productData._definition[0].details ? (productData._definition[0].details.find(detail => detail['display-name'] === 'Summary' || detail['display-name'] === 'Description')) : '';
      const productDescriptionValue = productDescription !== undefined ? productDescription['display-value'] : '';
      return (
        <div className="featured-product-component">
          {
                orientation === 'LEFT'
                && (
                  <div className="image-container">
                    <a href={`${window.location.origin}/product/sku/${productData._code[0].code}`}>
                      <img className="product-image left-image" alt="home-espot-3" src={Config.skuImagesUrl.replace('%sku%', productData._code[0].code)} onError={(e) => { e.target.src = imgPlaceholder; }} />
                    </a>
                  </div>
                )
              }

          <div className="text-container">
            <div className="content-box">
              <div>
                <span>
                  <a className="item-title" href={`${window.location.origin}/product/sku/${productData._code[0].code}`}>{productData._definition[0]['display-name']}</a>
                </span>
                <div className="text-box">
                  {productDescriptionValue}
                  <div className="gradient-box" />
                </div>
              </div>
              <div className="item-price-container">
                {
                  listPrice !== itemPrice
                    ? (
                      <span className="item-list-price line" data-region="itemListPriceRegion">
                        {listPrice}
                      </span>
                    )
                    : ('')
                }
                <span className="item-purchase-price line">
                  {itemPrice}
                </span>
              </div>
              <button
                className="ep-btn primary wide btn-itemdetail-addtocart"
                id="product_display_item_add_to_cart_button"
                type="submit"
                onClick={this.addToCart.bind(this)}
              >
                {intl.get('add-to-cart')}
              </button>
              {
                  (FeaturedProductMain.isLoggedIn() && !Object.keys(productData._addtocartform[0].configuration).length > 0) ? (
                    <button
                      className="ep-btn primary wide btn-itemdetail-addtowishlist"
                      id="product_display_item_wishlist_to_cart_button"
                      type="submit"
                      onClick={this.addToWishList.bind(this)}
                    >
                      {intl.get('add-to-wish-list')}
                    </button>
                  ) : ('')
                }
            </div>
          </div>
          {
            orientation === 'RIGHT'
            && (
              <div className="image-container">
                <a href={`${window.location.origin}/product/sku/${productData._code[0].code}`}>
                  <img className="product-image right-image" alt="home-espot-3" src={Config.skuImagesUrl.replace('%sku%', productData._code[0].code)} onError={(e) => { e.target.src = imgPlaceholder; }} />
                </a>
              </div>
            )
          }
        </div>
      );
    }

    return (
      <div className="featured-items-component" />
    );
  }
}

export default FeaturedProductMain;
