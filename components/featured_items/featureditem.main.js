/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */
import React from 'react';
import PropTypes from 'prop-types';

class FeaturedItemMain extends React.Component {
  static propTypes = {
    product: PropTypes.objectOf(PropTypes.object).isRequired,
  }

  constructor(props) {
    super(props);
    const { product } = this.props;
    this.state = {
      productData: product,
    };
  }

  render() {
    const { productData } = this.state;

    if (productData) {
      let listPrice = 'n/a';
      if (productData._price) {
        listPrice = productData._price[0]['list-price'][0].display;
      }
      let itemPrice = 'n/a';
      if (productData._price) {
        itemPrice = productData._price[0]['purchase-price'][0].display;
      }
      const productTitle = productData._definition[0]['display-name'];
      const productDescription = productData._definition[0].details
        ? (productData._definition[0].details.find(detail => detail['display-name'] === 'Summary' || detail['display-name'] === 'Description' || detail['display-name'] === 'Product Description'))
        : '';
      const productDescriptionValue = productDescription !== undefined ? productDescription['display-value'] : '';
      return (
        <div className="featured-item-info">
          <div className="item-name featured-item-text">{productTitle}</div>
          <div className="item-description featured-item-text">{productDescriptionValue}</div>
          <div className="item-price-container featured-item-text" data-region="itemPriceRegion">
            <div data-region="itemPriceRegion">
              <ul className="item-price-container">
                {
                  listPrice !== itemPrice
                    ? (
                      <li className="item-list-price" data-region="itemListPriceRegion">
                        {listPrice}
                      </li>
                    )
                    : ('')
                }
                <li className="item-purchase-price">
                  <h1 className="item-purchase-price-value">
                    {itemPrice}
                  </h1>
                </li>
              </ul>
            </div>
          </div>
        </div>
      );
    }

    return (
      null
    );
  }
}

export default FeaturedItemMain;
