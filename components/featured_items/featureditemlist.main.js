/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import Config from 'config';
import { login } from '../../common/AuthService';
import { itemLookup, cortexFetchItemLookupForm, userRecommendationsLookup } from '../../common/CortexLookup';
import FeaturedItemMain from './featureditem.main';

class FeaturedItemListMain extends React.Component {
  static isLoggedIn() {
    return (localStorage.getItem(`${Config.cortexApi.scope}_oAuthRole`) === 'REGISTERED');
  }

  static propTypes = {
    propertiesMap: PropTypes.objectOf(PropTypes.object).isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {
      productList: [],
    };
  }

  componentDidMount() {
    const { propertiesMap } = this.props;

    if (propertiesMap.fieldDefaultSku) {
      this.fetchRecommendations(propertiesMap.fieldDefaultSku);
    } else {
      this.populateProductList();
    }
  }


  generateSkuListFromInputtedSkus(skusToAdd = 3) {
    const { propertiesMap } = this.props;
    const skuList = [];

    switch (skusToAdd) {
      case 1:
        if (propertiesMap.skuLeft) {
          skuList.push(propertiesMap.skuLeft);
        }
        break;

      case 2:
        if (propertiesMap.skuLeft) {
          skuList.push(propertiesMap.skuLeft);
        }
        if (propertiesMap.skuCenter) {
          skuList.push(propertiesMap.skuCenter);
        }
        break;

      default:
        if (propertiesMap.skuLeft) {
          skuList.push(propertiesMap.skuLeft);
        }
        if (propertiesMap.skuCenter) {
          skuList.push(propertiesMap.skuCenter);
        }
        if (propertiesMap.skuRight) {
          skuList.push(propertiesMap.skuRight);
        }
        break;
    }

    return skuList;
  }

  makeupForMissingRecommendationsValues(profileRecommendations) {
    if (profileRecommendations.length < 3) {
      const skusToAdd = profileRecommendations.length - 3;
      if (skusToAdd) {
        return this.generateSkuListFromInputtedSkus(skusToAdd).concat(profileRecommendations);
      }
      return profileRecommendations;
    }

    return profileRecommendations.slice(0, 3);
  }

  populateProductList() {
    const { propertiesMap } = this.props;

    if (parseInt(propertiesMap.useSmartRecommendations, 10)) {
      login().then(() => {
        userRecommendationsLookup().then((res) => {
          const skuList = this.makeupForMissingRecommendationsValues(res);
          this.fetchProducts(skuList);
        });
      });
    } else {
      const skuList = this.generateSkuListFromInputtedSkus();
      this.fetchProducts(skuList);
    }
  }

  fetchRecommendations(sku) {
    login().then(() => {
      cortexFetchItemLookupForm().then(() => {
        itemLookup(sku).then((res) => {
          if (res._recommendations[0]._crosssell[0]._element) {
            this.setState({
              productList: res._recommendations[0]._crosssell[0]._element.slice(0, 3),
            });
          } else {
            const skuList = this.generateSkuListFromInputtedSkus();
            this.fetchProducts(skuList);
          }
        })
          .catch((error) => {
            /* eslint-disable-next-line no-console */
            console.error(error.message);
          });
      });
    });
  }

  fetchProducts(skuList) {
    login().then(() => {
      cortexFetchItemLookupForm().then(() => {
        skuList.forEach((sku) => {
          itemLookup(sku).then((res) => {
            this.setState(prevState => ({
              productList: [...prevState.productList, res],
            }));
          })
            .catch((error) => {
              /* eslint-disable-next-line no-console */
              console.error(error.message);
            });
        });
      });
    });
  }

  renderFeaturedItems() {
    const { productList } = this.state;
    const numProducts = productList.length;
    const itemClass = `featured-item-container col-lg-${12 / numProducts} col-md-${numProducts > 1 ? 6 : 12} col-xs-12 col-center`;
    return productList.map(product => (
      <a className={itemClass} href={`${window.location.origin}/product/sku/${product._code[0].code}`}>
        <FeaturedItemMain product={product} />
      </a>
    ));
  }

  render() {
    const { productList } = this.state;

    if (productList.length > 0) {
      return (
        <div className="featured-items-component row row-center">
          {this.renderFeaturedItems()}
        </div>
      );
    }

    return (
      <div className="featured-items-component" />
    );
  }
}

export default FeaturedItemListMain;
