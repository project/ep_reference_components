/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Config from 'config';
// import { withRouter } from 'react-router';
import intl from 'react-intl-universal';
import { login } from '../../common/AuthService';
import { cortexFetch } from '../../common/Cortex';

class CheckoutSummaryCommercial extends React.Component {
  static propTypes = {
    data: PropTypes.objectOf(PropTypes.any).isRequired,
    onChange: PropTypes.func,
  }

  static defaultProps = {
    onChange: () => {},
  }

  deletePromotionCode(link) {
    login().then(() => {
      cortexFetch(link, {
        method: 'delete',
        headers: {
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
        },
      }).then(() => {
        const { onChange } = this.props;
        onChange();
      }).catch((error) => {
        // eslint-disable-next-line no-console
        console.error(error.message);
      });
    });
  }

  renderCoupons() {
    const { data } = this.props;
    if (data._order && data._order[0] && data._order[0]._couponinfo[0]._coupon) {
      return (
        <div className="cart-coupons" data-region="cartAppliedPromotionsRegion">
          <div className="cart-summary-label-col">
            {intl.get('applied-coupons')}
          </div>
          {data._order[0]._couponinfo[0]._coupon.map(coupon => (
            <div className="promotions-table" key={coupon.code}>
              <div className="row">
                <div className="col-6 promotion-display">
                  {coupon.code}
                </div>
                <div className="col-6 promotion-remove">
                  <button type="button" className="cart-remove-promotion" onClick={() => { this.deletePromotionCode(coupon.self.uri); }} data-actionlink="">
                    {intl.get('remove')}
                  </button>
                </div>
              </div>
            </div>
          ))}
          <hr />
        </div>
      );
    }
    return null;
  }

  renderPromotions() {
    const { data } = this.props;
    if (data._appliedpromotions) {
      return (
        <div className="cart-applied-promotions" data-region="cartAppliedPromotionsRegion">
          <div className="cart-summary-label-col">
            {intl.get('applied-promotions')}
          </div>
          {data._appliedpromotions[0]._element.map(promotion => (
            <div className="cart-summary-value-col cart-applied-promotions" key={promotion.name}>
              {promotion['display-name']}
            </div>
          ))}
          <hr />
        </div>
      );
    }
    return null;
  }

  renderDiscount() {
    const { data } = this.props;
    if (data._discount) {
      return (
        <div className="cart-discount checkout-summary-row">
          <div>
            {intl.get('todays-discount')}
          </div>
          <div>
            {data._discount[0].discount[0].display}
          </div>
        </div>
      );
    }
    return null;
  }

  renderShipping() {
    const { data } = this.props;
    if (data._order && data._order[0]._deliveries && data._order[0]._deliveries[0]._element[0]._shippingoptioninfo) {
      const shippingOption = (data._order[0]._deliveries[0]._element[0]._shippingoptioninfo[0]._shippingoption) ? (data._order[0]._deliveries[0]._element[0]._shippingoptioninfo[0]._shippingoption[0].cost[0].display) : ('');
      return (
        <div className="checkout-shipping checkout-summary-row">
          <div>
            {intl.get('todays-shipping-cost')}
          </div>
          <div>
            {shippingOption}
          </div>
        </div>
      );
    }
    return null;
  }

  renderTax() {
    const { data } = this.props;
    if (data._order && data._order[0]._tax && data._order[0]._tax[0].cost.length) {
      return (
        <div className="checkout-tax">
          {data._order[0]._tax[0].cost.map(tax => (
            <div className="checkout-tax checkout-summary-row" key={tax.title}>
              <div>
                {tax.title}
              </div>
              <div>
                {tax.display}
              </div>
            </div>
          ))}
        </div>
      );
    }
    return null;
  }

  renderCheckoutTotal() {
    const { data } = this.props;
    if (data._order && data._order[0]._total) {
      return (
        <div className="checkout-total checkout-summary-row">
          <div>
            {intl.get('todays-total')}
          </div>
          <div data-el-value="checkout.total">
            {data._order[0]._total[0].cost[0].display}
          </div>
        </div>
      );
    }
    return null;
  }

  render() {
    const { data } = this.props;
    return (
      <div className="checkout-summary-commercial">
        {this.renderPromotions()}
        {this.renderCoupons()}
        <div className="cart-total-quantity checkout-summary-row">
          <div>
            {intl.get('total-quantity')}
          </div>
          <div>
            {data['total-quantity']}
          </div>
        </div>
        <div className="cart-subtotal checkout-summary-row">
          <div>
            {intl.get('todays-subtotal')}
          </div>
          <div>
            {data._total[0].cost[0].display}
          </div>
        </div>
        {this.renderDiscount()}
        {this.renderShipping()}
        {this.renderTax()}
        {this.renderCheckoutTotal()}
      </div>
    );
  }
}

export default CheckoutSummaryCommercial;
