/**
 * Copyright © 2018 Elastic Path Software Inc. All rights reserved.
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this license. If not, see
 *
 *     https://www.gnu.org/licenses/
 *
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-responsive-modal';
import Config from 'config';
import Papa from 'papaparse';
import intl from 'react-intl-universal';
import { login } from '../../common/AuthService';
import { cortexFetch } from '../../common/Cortex';

const zoomArray = [
  'defaultcart:batch-add-to-cart-form',
];

class BatchQuickOrderModalMain extends React.Component {
  static propTypes = {
    handleModalClose: PropTypes.func.isRequired,
    openModal: PropTypes.bool.isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {
      cartData: undefined,
    };
    this.parseParams = this.parseParams.bind(this);
  }

  componentDidMount() {
    this.fetchCartData();
  }

  componentWillReceiveProps() {
    this.fetchCartData();
  }

  fetchCartData() {
    login().then(() => {
      cortexFetch(`/?zoom=${zoomArray.sort().join()}`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
        },
      })
        .then(res => res.json())
        .then((res) => {
          this.setState({
            cartData: res._defaultcart[0],
          });
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error.message);
        });
    });
  }

  // eslint-disable-next-line class-methods-use-this
  parseParams(event) {
    event.preventDefault();
    let body = 'item-code,quantity\n';
    const stringList = [];
    for (let i = 1; i <= 5; i++) {
      const sku = document.getElementById(`sku-input-${i}`).value;
      const qty = document.getElementById(`qty-input-${i}`).value;
      if (sku !== '' && qty !== '') {
        stringList.push(`${sku},${qty}`);
      } else if (sku !== '' || qty !== '') {
        document.getElementById('batch-quick-order-error').innerHTML = intl.get('missing-sku-quantity');
        return;
      }
    }
    if (stringList.length !== 0) {
      for (let i = 0; i < stringList.length; i++) {
        body += `${stringList[i]}\n`;
      }
      Papa.parse(body, {
        header: true,
        dynamicTyping: true,
        encoding: 'utf-8',
        skipEmptyLines: true,
        // eslint-disable-next-line func-names
        complete: (function (results) {
          // eslint-disable-next-line no-console
          console.log(results.data);
          this.batchAddToCart(results.data);
        }).bind(this),
      });
    } else {
      document.getElementById('batch-quick-order-error').innerHTML = intl.get('no-pair-values');
    }
  }

  batchAddToCart(list) {
    const body = {
      items: list,
    };
    const { cartData } = this.state;
    login().then(() => {
      const addToCartLink = cartData['_batch-add-to-cart-form'][0].links.find(link => link.rel === 'submit-action');
      cortexFetch(addToCartLink.uri,
        {
          method: 'post',
          headers: {
            'Content-Type': 'application/json',
            Authorization: localStorage.getItem(`${Config.cortexApi.scope}_oAuthToken`),
          },
          body: JSON.stringify(body),
        })
        .then((res) => {
          if (res.status === 200 || res.status === 201) {
            window.location = '/mybag';
          } else {
            let debugMessages = '';
            res.json().then((json) => {
              for (let i = 0; i < json.messages.length; i++) {
                debugMessages = debugMessages.concat(`- ${json.messages[i]['debug-message']} \n `);
              }
              document.getElementById('batch-quick-order-error').innerHTML = debugMessages;
            });
          }
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error.message);
        });
    }).catch((error) => {
      // eslint-disable-next-line no-console
      console.error(error.message);
    });
  }

  render() {
    const { handleModalClose, openModal } = this.props;

    return (
      <Modal open={openModal} onClose={handleModalClose} classNames={{ modal: 'quick-order-modal-content' }}>
        <div id="quick-order-modal">
          <div className="modal-content" id="simplemodal-container">

            <div className="modal-header">
              <h2 className="modal-title">
                {intl.get('quick-order-entry')}
              </h2>
            </div>

            <div className="feedback-label auth-feedback-container" id="batch-quick-order-error" data-region="authLoginFormFeedbackRegion" data-i18n="" />

            <div className="modal-body">
              <form id="quick_order_modal_form" onSubmit={this.parseParams}>
                <div className="quick-order-form-row">
                  <div className="form-group quick-order-form-col">
                    <span>
                      {intl.get('sku')}
                      :
                    </span>
                    <input className="form-control" id="sku-input-1" type="text" />
                  </div>
                  <div className="form-group quick-order-form-col">
                    <span>
                      {intl.get('quantity')}
                      :
                    </span>
                    <input className="form-control" id="qty-input-1" type="number" min="1" />
                  </div>
                </div>
                <div className="quick-order-form-row">
                  <div className="form-group quick-order-form-col">
                    <span>
                      {intl.get('sku')}
                      :
                    </span>
                    <input className="form-control" id="sku-input-2" type="text" />
                  </div>
                  <div className="form-group quick-order-form-col">
                    <span>
                      {intl.get('quantity')}
                      :
                    </span>
                    <input className="form-control" id="qty-input-2" type="number" min="1" />
                  </div>
                </div>
                <div className="quick-order-form-row">
                  <div className="form-group quick-order-form-col">
                    <span>
                      {intl.get('sku')}
                      :
                    </span>
                    <input className="form-control" id="sku-input-3" type="text" />
                  </div>
                  <div className="form-group quick-order-form-col">
                    <span>
                      {intl.get('quantity')}
                      :
                    </span>
                    <input className="form-control" id="qty-input-3" type="number" min="1" />
                  </div>
                </div>
                <div className="quick-order-form-row">
                  <div className="form-group quick-order-form-col">
                    <span>
                      {intl.get('sku')}
                      :
                    </span>
                    <input className="form-control" id="sku-input-4" type="text" />
                  </div>
                  <div className="form-group quick-order-form-col">
                    <span>
                      {intl.get('quantity')}
                      :
                    </span>
                    <input className="form-control" id="qty-input-4" type="number" min="1" />
                  </div>
                </div>
                <div className="quick-order-form-row">
                  <div className="form-group quick-order-form-col">
                    <span>
                      {intl.get('sku')}
                      :
                    </span>
                    <input className="form-control" id="sku-input-5" type="text" />
                  </div>
                  <div className="form-group quick-order-form-col">
                    <span>
                      {intl.get('quantity')}
                      :
                    </span>
                    <input className="form-control" id="qty-input-5" type="number" min="1" />
                  </div>
                </div>
                <div className="form-group action-row">
                  <div className="form-input btn-container">
                    <button className="ep-btn" id="quick_order_modal_submit_button" data-cmd="login" data-toggle="collapse" data-target=".navbar-collapse" type="submit">
                      {intl.get('submit')}
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

export default BatchQuickOrderModalMain;
